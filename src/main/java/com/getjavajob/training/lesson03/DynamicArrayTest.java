package com.getjavajob.training.algo08.tenetkok.lesson03;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class DynamicArrayTest {
    public static void main(String[] args) {
        testDynamicArray();
        testAddObject();
        testAddIObject();
        testAddToBegin();
        testAddToMiddle();
        testAddToEnd();
        testSet();
        testGet();
        testRemoveObject();
        testRemoveIObject();
        testRemoveFromBeginning();
        testRemoveFromMiddle();
        testRemoveFromEnd();
        testSize();
        testContains();
        testIndexOf();
    }

    public static void testDynamicArray() {
        String msg = "Test of DynamicArray(int i) failed";
        try {
            DynamicArray intDynamicArray = new DynamicArray(-1);
            fail(msg);
        } catch (ArrayIndexOutOfBoundsException e1) {
            assertEquals("DynamicArrayTest.testDynamicArrayFail", "Illegal initial capacity, index < 0", e1.getMessage());
        }
    }

    public static void testAddObject() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        Object[] result = {0, 1, 2};
        assertEquals("DynamicArrayTest.testAddObject", result, intDynamicArray.toArray());
    }

    public static void testAddIObject() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }

        String msg = "Test of add(int i, Object e) failed";
        try {
            intDynamicArray.add(4, 4);
            fail(msg);
        } catch (ArrayIndexOutOfBoundsException e1) {
            assertEquals("DynamicArrayTest.testAddIObject", "Illegal index: 4", e1.getMessage());
        }
    }

    public static void testAddToBegin() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        intDynamicArray.add(0, 3);
        Object[] result = {3, 0, 1, 2};
        assertEquals("DynamicArrayTest.testAddToBegin", result, intDynamicArray.toArray());
    }

    public static void testAddToMiddle() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        intDynamicArray.add(intDynamicArray.size() / 2, 3);
        Object[] result = {0, 3, 1, 2};
        assertEquals("DynamicArrayTest.testAddToMiddle", result, intDynamicArray.toArray());
    }

    public static void testAddToEnd() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        intDynamicArray.add(intDynamicArray.size() - 1, 3);
        Object[] result = {0, 1, 3, 2};
        assertEquals("DynamicArrayTest.testAddToEnd", result, intDynamicArray.toArray());
    }


    public static void testSet() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        intDynamicArray.set(1, 0);
        Object[] result = {0, 0, 2};
        assertEquals("DynamicArrayTest.testSet", result, intDynamicArray.toArray());

        String msg = "Test set(int i, Object e) failed";
        try {
            intDynamicArray.set(4, 5);
            fail(msg);
        } catch (ArrayIndexOutOfBoundsException e1) {
            assertEquals("DynamicArrayTest.testSetFail", "Illegal index: 4", e1.getMessage());
        }
    }

    public static void testGet() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        assertEquals("DynamicArrayTest.testGet", 2, intDynamicArray.get(2));

        String msg = "Test get(int i, Object e) failed";
        try {
            intDynamicArray.get(4);
            fail(msg);
        } catch (ArrayIndexOutOfBoundsException e1) {
            assertEquals("DynamicArrayTest.testGetFail", "Illegal index: 4", e1.getMessage());
        }
    }

    public static void testRemoveObject() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }

        assertEquals("DynamicArrayTest.testRemoveObjectRemoved1", true, intDynamicArray.remove((Integer) 1));
        Object[] result = {0, 2};
        assertEquals("DynamicArrayTest.testRemoveObjectResult", result, intDynamicArray.toArray());
        assertEquals("DynamicArrayTest.testRemoveObjectRemoved2", false, intDynamicArray.remove((Integer) 6));
    }

    public static void testRemoveIObject() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }

        String msg = "Test remove(int i) failed";
        try {
            intDynamicArray.remove(3);
            fail(msg);
        } catch (ArrayIndexOutOfBoundsException e1) {
            assertEquals("DynamicArrayTest.testRemoveIObjectFail", "Illegal index: 3", e1.getMessage());

        }
    }

    public static void testRemoveFromBeginning() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        intDynamicArray.remove(0);
        Object[] result = {1, 2};
        assertEquals("DynamicArrayTest.testRemoveFromBeginning", result, intDynamicArray.toArray());
    }

    public static void testRemoveFromMiddle() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        intDynamicArray.remove(intDynamicArray.size() / 2);
        Object[] result = {0, 2};
        assertEquals("DynamicArrayTest.testRemoveFromMiddle", result, intDynamicArray.toArray());
    }

    public static void testRemoveFromEnd() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        intDynamicArray.remove(intDynamicArray.size() - 1);
        Object[] result = {0, 1};
        assertEquals("DynamicArrayTest.testRemoveFromEnd", result, intDynamicArray.toArray());
    }


    public static void testSize() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        assertEquals("DynamicArrayTest.testSize", 3, intDynamicArray.size());
    }

    public static void testIndexOf() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        assertEquals("DynamicArrayTest.testIndexOfObject1", 2, intDynamicArray.indexOf(2));
        assertEquals("DynamicArrayTest.testIndexOfObject2", -1, intDynamicArray.indexOf(4));
    }

    public static void testContains() {
        DynamicArray intDynamicArray = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            intDynamicArray.add(i);
        }
        assertEquals("DynamicArrayTest.testContainsObject1", true, intDynamicArray.contains(2));
        assertEquals("DynamicArrayTest.testContainsObject2", false, intDynamicArray.contains(5));
    }
}
