package com.getjavajob.training.algo08.tenetkok.lesson03;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo08.util.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.util.StopWatch.start;

public class DynamicArrayPerformanceTest {

    static final int NUMBER_OF_ELEMENTS = 50_000_000;

    public static void main(String[] args) {
        testAddToBeginDA();
        testAddToBeginAL();
        testAddToMiddleDA();
        testAddToMiddleAL();
        testAddToEndDA();
        testAddToEndAL();
        testRemoveFromBeginningDA();
        testRemoveFromBeginningAL();
        testRemoveFromMiddleDA();
        testRemoveFromMiddleAL();
        testRemoveFromEndDA();
        testRemoveFromEndAL();
    }

    public static void testAddToBeginDA() {
        System.out.println("--------Addition to the beginning--------");
        DynamicArray array = new DynamicArray();
        array.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            array.add(0, i);
        }
        System.out.println("DynamicArray.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToBeginAL() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(0, i);
        }
        System.out.println("ArrayList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToMiddleDA() {
        System.out.println("--------Addition to the middle--------");
        DynamicArray array = new DynamicArray();
        for (int i = 0; i < 3; i++) {
            array.add(0);
        }
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            array.add(array.size() / 2, i);
        }
        System.out.println("DynamicArray.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToMiddleAL() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            list.add(0);
        }
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(list.size() / 2, i);
        }
        System.out.println("ArrayList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToEndDA() {
        System.out.println("--------Addition to the end--------");
        DynamicArray array = new DynamicArray();
        array.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            array.add(array.size() - 1, i);
        }
        System.out.println("DynamicArray.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToEndAL() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(list.size() - 1, i);
        }
        System.out.println("ArrayList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromBeginningDA() {
        System.out.println("--------Remove from the beginning--------");
        DynamicArray array = new DynamicArray();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.remove(0);
        }
        System.out.println("DynamicArray.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromBeginningAL() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(0);
        }
        System.out.println("ArrayList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromMiddleDA() {
        System.out.println("--------Remove from the middle--------");
        DynamicArray array = new DynamicArray();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.remove(array.size() / 2);
        }
        System.out.println("DynamicArray.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromMiddleAL() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(list.size() / 2);
        }
        System.out.println("ArrayList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromEndDA() {
        System.out.println("--------Remove from the end--------");
        DynamicArray array = new DynamicArray();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.remove(array.size() - 1);
        }
        System.out.println("DynamicArray.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromEndAL() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(list.size() - 1);
        }
        System.out.println("ArrayList.remove(e): " + getElapsedTime() + " ms");
    }
}

