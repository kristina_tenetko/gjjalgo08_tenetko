package com.getjavajob.training.algo08.tenetkok.lesson03;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

public class DynamicArray {

    private static final int DEFAULT_CAPACITY = 10;
    private Object[] data;
    private int size;
    private int modificationCount;

    public DynamicArray(int capacity) {
        if (capacity < 0) {
            throw new ArrayIndexOutOfBoundsException("Illegal initial capacity, index < 0");
        }
        data = new Object[capacity];
    }

    public DynamicArray() {
        this(DEFAULT_CAPACITY);
    }

    private void checkIndexOutOfBounds(int index) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("Illegal index: " + index);
        }
    }

    private void checkIndexOutOfBoundsForAdd(int index) {
        if (index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Illegal index: " + index);
        }
    }

    private void growIfFull() {
        if (size + 1 > data.length) {
            Object[] bigger = new Object[(int) (data.length * 1.5)];
            System.arraycopy(data, 0, bigger, 0, data.length);
            data = bigger;
        }
    }

    boolean add(Object e) {
        growIfFull();
        modificationCount++;
        data[size++] = e;
        return true;
    }

    void add(int i, Object e) {
        checkIndexOutOfBoundsForAdd(i);
        growIfFull();
        modificationCount++;
        if (i != size) {
            System.arraycopy(data, i, data, i + 1, size - i);
        }
        data[i] = e;
        size++;

    }

    Object set(int i, Object e) {
        checkIndexOutOfBounds(i);
        Object oldValue = get(i);
        data[i] = e;
        return oldValue;
    }

    Object get(int i) {
        checkIndexOutOfBounds(i);
        return data[i];
    }

    Object remove(int i) {
        checkIndexOutOfBounds(i);
        Object value = data[i];
        if (i < size - 1) {
            System.arraycopy(data, i + 1, data, i, size - i - 1);
        }
        data[--size] = null;
        modificationCount++;
        return value;
    }

    boolean remove(Object e) {
        int value = indexOf(e);
        if (value == -1) {
            return false;
        }
        remove(value);
        return true;
    }

    int size() {
        return size;
    }

    int indexOf(Object e) {
        for (int i = 0; i < size; i++) {
            if (e == null && data[i] == null) {
                return i;
            }
            if (data[i].equals(e)) {
                return i;
            }
        }
        return -1;
    }

    boolean contains(Object e) {
        return indexOf(e) != -1;
    }

    Object[] toArray() {
        return Arrays.copyOf(data, size);
    }

    public ListIterator listIterator() {
        return new ListIterator(0);
    }

    public ListIterator listIterator(int index) {
        return new ListIterator(index);
    }

    public class ListIterator {
        int nextIndex;
        int lastReturned = -1;
        private int expectedModificationCount = modificationCount;

        public ListIterator(int index) {
            nextIndex = index;
        }

        public boolean hasNext() {
            return nextIndex < size;
        }

        public Object next() {
            checkConcurrentModification();
            if (!hasNext()) {
                throw new NoSuchElementException("Iterator bounds reached");
            }
            Object[] array = DynamicArray.this.data;
            int i = nextIndex;
            if (i >= data.length) {
                throw new ConcurrentModificationException();
            } else {
                nextIndex++;
                lastReturned = i;
                return array[lastReturned];
            }
        }

        public boolean hasPrevious() {
            return nextIndex != 0;
        }

        public Object previous() {
            checkConcurrentModification();
            int i = nextIndex - 1;
            if (i < 0) {
                throw new NoSuchElementException("Iterator bounds reached");
            } else {
                Object[] array = DynamicArray.this.data;
                if (i >= array.length) {
                    throw new ConcurrentModificationException();
                } else {
                    nextIndex = i;
                    lastReturned = i;
                    return array[lastReturned = i];
                }
            }
        }

        public int nextIndex() {
            return nextIndex;
        }

        public int previousIndex() {
            return nextIndex - 1;
        }

        public void remove() {
            if (lastReturned < 0) {
                throw new IllegalStateException("Cannot remove element");
            }
            checkConcurrentModification();
            try {
                DynamicArray.this.remove(lastReturned);
                nextIndex = lastReturned;
                lastReturned = -1;
                expectedModificationCount = modificationCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void set(Object e) {
            if (lastReturned < 0) {
                throw new IllegalStateException("Cannot set element");
            }
            checkConcurrentModification();
            try {
                DynamicArray.this.set(lastReturned, e);
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void add(Object e) {
            checkConcurrentModification();
            try {
                int i = nextIndex;
                DynamicArray.this.add(i, e);
                nextIndex = i + 1;
                lastReturned = -1;
                expectedModificationCount = modificationCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        private void checkConcurrentModification() {
            if (modificationCount != expectedModificationCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
