package com.getjavajob.training.algo08.tenetkok.lesson07.binary;

import com.getjavajob.training.algo08.tenetkok.lesson07.Node;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class LinkedBinaryTreeTest {

    public static void main(String[] args) {
        testAddRoot();
        testAdd();
        testAddLeft();
        testAddRight();
        testSet();
        testSize();
        testRemove();
        testLeft();
        testRight();
        testParent();
        testPreOrder();
        testPostOrder();
        testInorder();
        testBreadthFirst();
    }

    private static void testRight() {
        BinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Value1");

        assertEquals("LinkedBinaryTree.testRight()", right, tree.right(root));
        assertEquals("LinkedBinaryTree.testRight()", null, tree.right(right));
    }

    private static void testLeft() {
        BinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> left = tree.addLeft(root, "Value1");

        assertEquals("LinkedBinaryTree.testLeft()", left, tree.left(root));
        assertEquals("LinkedBinaryTree.testLeft()", null, tree.left(left));
    }

    private static void testParent() {
        BinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Value1");
        Node<String> left = tree.addLeft(root, "Value2");

        assertEquals("LinkedBinaryTree.testParent()", root, tree.parent(right));
        assertEquals("LinkedBinaryTree.testParent()", root, tree.parent(left));
        assertEquals("LinkedBinaryTree.testParent()", null, tree.parent(root));
    }

    private static void testSize() {
        BinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Value1");
        Node<String> right2 = tree.addRight(right, "Value2");
        Node<String> left = tree.addRight(right2, "Value3");

        assertEquals("LinkedBinaryTree.testSize()", 4, tree.size());
    }

    private static void testSet() {
        BinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Value");
        String oldValue = tree.set(right, "NewValue");

        assertEquals("LinkedBinaryTree.testSet()", "Value", oldValue);
        assertEquals("LinkedBinaryTree.testSet()", "NewValue", tree.right(root).getElement());

    }

    private static void testAddRight() {
        BinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Right");

        assertEquals("LinkedBinaryTree.testAddLeft()", right, tree.right(root));

        try {
            tree.addRight(root, "newR");
            fail("LinkedBinaryTree.testAdd() failed");
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTree.testAdd()", "This node already has right child", e.getMessage());
        }
    }

    private static void testAddLeft() {
        BinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> left = tree.addLeft(root, "Left");

        assertEquals("LinkedBinaryTree.testAddLeft()", left, tree.left(root));

        try {
            tree.addLeft(root, "newLeft");
            fail("LinkedBinaryTree.testAdd() failed");
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTree.testAdd()", "This node already has left child", e.getMessage());
        }
    }

    private static void testRemove() {
        BinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Value1");
        Node<String> right2 = tree.addRight(right, "Value2");
        Node<String> left2 = tree.addLeft(right, "Value3");

        try {
            tree.remove(right);
            fail("LinkedBinaryTree.testRemove() failed");
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTree.testRemove()", "Can't remove node, because node has two child", e.getMessage());
        }
        String value = tree.remove(right2);

        assertEquals("LinkedBinaryTree.testRemove()", "Value2", value);
        assertEquals("LinkedBinaryTree.testRemove()", null, tree.right(right));
    }

    private static void testAdd() {
        BinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> left = tree.add(root, "Left");
        assertEquals("LinkedBinaryTree.testAdd()", left, tree.left(root));
        Node<String> right = tree.add(root, "Right");
        assertEquals("LinkedBinaryTree.testAdd()", right, tree.right(root));

        try {
            tree.add(root, "w");
            fail("LinkedBinaryTree.testAdd() failed");
        } catch (IllegalArgumentException e) {
            assertEquals("LinkedBinaryTree.testAdd()", "Node already has two children", e.getMessage());
        }
    }

    private static void testAddRoot() {
        BinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("Root");

        assertEquals("LinkedBinaryTree.testAddRoot", "Root", root.getElement());
        assertEquals("LinkedBinaryTree.testRoot()", root, tree.root());

        try {
            tree.addRoot("NewRoot");
            fail("LinkedBinaryTree.testAddRoot failed");
        } catch (IllegalStateException e) {
            assertEquals("LinkedBinaryTree.testAddRootFail", "Tree already has root element", e.getMessage());
        }
    }

    private static void testBreadthFirst() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("F");
        tree.addRight(root, "G");
        tree.addLeft(root, "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");
        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");
        List<String> res = new ArrayList<>();
        List<String> cmp = new ArrayList<String>() {{
            add("F");
            add("B");
            add("G");
            add("A");
            add("D");
            add("I");
            add("C");
            add("E");
            add("H");
        }};
        for (Node<String> n : tree.breadthFirst()) {
            res.add(n.getElement());
        }
        assertEquals("LinkedBinaryTreeTest.testBreadthFirst", res, cmp);
    }

    private static void testPostOrder() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        Node root = tree.addRoot("F");
        tree.addRight(root, "G");
        tree.addLeft(root, "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");
        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");
        List<String> res = new ArrayList<>();
        List<String> cmp = new ArrayList<String>() {{
            add("A");
            add("C");
            add("E");
            add("D");
            add("B");
            add("H");
            add("I");
            add("G");
            add("F");
        }};
        for (Node<String> n : tree.postOrder()) {
            res.add(n.getElement());
        }
        assertEquals("LinkedBinaryTreeTest.testPostOrder()", res, cmp);
    }

    private static void testInorder() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("F");
        tree.addRight(root, "G");
        tree.addLeft(root, "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");
        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");
        List<String> res = new ArrayList<>();
        List<String> cmp = new ArrayList<String>() {{
            add("A");
            add("B");
            add("C");
            add("D");
            add("E");
            add("F");
            add("G");
            add("H");
            add("I");
        }};
        for (Node<String> n : tree.inOrder()) {
            res.add(n.getElement());
        }
        assertEquals("LinkedBinaryTreeTest.testInorder", res, cmp);
    }

    private static void testPreOrder() {
        LinkedBinaryTree<String> tree = new LinkedBinaryTree<>();
        Node<String> root = tree.addRoot("F");
        tree.addRight(root, "G");
        tree.addLeft(root, "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");
        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");
        List<String> res = new ArrayList<>();
        List<String> cmp = new ArrayList<String>() {{
            add("F");
            add("B");
            add("A");
            add("D");
            add("C");
            add("E");
            add("G");
            add("I");
            add("H");
        }};
        for (Node<String> n : tree.preOrder()) {
            res.add(n.getElement());
        }
        assertEquals("LinkedBinaryTreeTest.testPreOrder", res, cmp);
    }
}
