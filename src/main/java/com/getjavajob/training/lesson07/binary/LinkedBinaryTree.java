package com.getjavajob.training.algo08.tenetkok.lesson07.binary;

import com.getjavajob.training.algo08.tenetkok.lesson07.Node;

import java.util.*;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {

    // nonpublic utility

    protected Node<E> root;

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    protected NodeImpl<E> createNode(E e) {
        return new NodeImpl<>(e);
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root != null) {
            throw new IllegalStateException("Tree already has root element");
        }
        root = createNode(e);
        return root;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.getLeft() == null) {
            return addLeft(node, e);
        } else if (node.getRight() == null) {
            return addRight(node, e);
        } else {
            throw new IllegalArgumentException("Node already has two children");
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.getLeft() != null) {
            throw new IllegalArgumentException("This node already has left child");
        }
        NodeImpl<E> leftNode = createNode(e);
        node.setLeft(leftNode);
        leftNode.setParent(node);
        return leftNode;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.getRight() != null) {
            throw new IllegalArgumentException("This node already has right child");
        }
        NodeImpl<E> rightNode = createNode(e);
        node.setRight(rightNode);
        rightNode.setParent(node);
        return rightNode;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        E oldValue = node.getElement();
        node.setElement(e);
        return oldValue;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);

        if (node.getLeft() != null && node.getRight() != null) {
            throw new IllegalArgumentException("Can't remove node, because node has two child");
        }

        if (node.getParent() != null) {
            NodeImpl<E> parentNode = validate(node.getParent());
            if (parentNode.getLeft() == node) {
                if (node.getLeft() != null) {
                    parentNode.setLeft(node.getLeft());
                    parentNode.getLeft().setParent(parentNode);
                } else if (node.getRight() != null) {
                    parentNode.setLeft(node.getRight());
                    parentNode.getLeft().setParent(parentNode);
                } else {
                    parentNode.setLeft(null);
                }
            } else if (parentNode.getRight() == node) {
                if (node.getLeft() != null) {
                    parentNode.setRight(node.getLeft());
                    parentNode.getRight().setParent(parentNode);
                } else if (node.getRight() != null) {
                    parentNode.setRight(node.getRight());
                    parentNode.getRight().setParent(parentNode);
                } else {
                    parentNode.setRight(null);
                }
            }
        }
        return node.getElement();
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> node = validate(p);
        return node.getLeft();
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> node = validate(p);
        return node.getRight();
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        return node.getParent();
    }

    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.getParent() != null) {
            return node.isLeftChild() ? node.getParent().getRight() : node.getParent().getLeft();
        } else {
            return null;
        }
    }

    @Override
    public int size() {
        return nodes().size();
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    public Collection<Node<E>> inOrder() {
        if (root == null) {
            return null;
        }
        ArrayList<Node<E>> list = new ArrayList<>();
        Stack<NodeImpl<E>> s = new Stack<>();
        NodeImpl<E> currentNode = validate(root);
        while (!s.empty() || currentNode != null) {
            if (currentNode != null) {
                s.push(currentNode);
                currentNode = currentNode.left;
            } else {
                NodeImpl<E> n = s.pop();
                list.add(n);
                currentNode = n.right;
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> preOrder() {
        ArrayList<Node<E>> list = new ArrayList<>();
        if (root == null) {
            return list;
        }

        Stack<NodeImpl<E>> stack = new Stack<>();
        stack.push(validate(root));
        while (!stack.empty()) {
            NodeImpl<E> n = stack.pop();
            list.add(n);

            if (n.right != null) {
                stack.push(n.right);
            }
            if (n.left != null) {
                stack.push(n.left);
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> postOrder() {
        ArrayList<Node<E>> res = new ArrayList<>();
        if (root == null) {
            return res;
        }

        Stack<NodeImpl<E>> stack = new Stack<>();
        stack.push(validate(root));

        while (!stack.isEmpty()) {
            NodeImpl<E> temp = stack.peek();
            if (temp.left == null && temp.right == null) {
                NodeImpl<E> pop = stack.pop();
                res.add(pop);
            } else {
                if (temp.right != null) {
                    stack.push(temp.right);
                    temp.right = null;
                }
                if (temp.left != null) {
                    stack.push(temp.left);
                    temp.left = null;
                }
            }
        }
        return res;
    }

    @Override
    public Collection<Node<E>> breadthFirst() {
        ArrayList<Node<E>> breadthList = new ArrayList<>();
        if (root == null) {
            System.out.println("Empty tree");
        } else {
            Queue<Node<E>> q = new LinkedList<>();
            q.add(root());
            while (q.peek() != null) {
                NodeImpl<E> temp = (NodeImpl<E>) q.remove();
                breadthList.add(temp);
                if (temp.left != null) {
                    q.add(temp.left);
                }
                if (temp.right != null) {
                    q.add(temp.right);
                }
            }
        }
        return breadthList;
    }


    @Override
    public Collection<Node<E>> nodes() {
        return breadthFirst();
    }

    protected static class NodeImpl<E> implements Node<E> {
        E value;
        NodeImpl<E> parent;
        NodeImpl<E> left;
        NodeImpl<E> right;


        public NodeImpl(E value) {
            this.value = value;
        }

        public NodeImpl<E> getParent() {
            return parent;
        }

        public void setParent(NodeImpl<E> parent) {
            this.parent = parent;
        }

        public NodeImpl<E> getLeft() {
            return left;
        }

        public void setLeft(NodeImpl<E> left) {
            this.left = left;
        }

        public NodeImpl<E> getRight() {
            return right;
        }

        public void setRight(NodeImpl<E> right) {
            this.right = right;
        }

        public boolean isLeftChild() {
            return (parent != null) && parent.left == this;
        }

        public boolean isRightChild() {
            return (parent != null) && parent.right == this;
        }

        @Override
        public E getElement() {
            return value;
        }

        public void setElement(E e) {
            value = e;
        }
    }
}