package com.getjavajob.training.algo08.tenetkok.lesson07.binary;

import com.getjavajob.training.algo08.tenetkok.lesson07.Node;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class ArrayBinaryTreeTest {

    public static void main(String[] args) {
        testAddRoot();
        testAdd();
        testAddLeft();
        testAddRight();
        testSet();
        testSize();
        testRemove();
        testLeft();
        testRight();
        testParent();
    }

    private static void testRight() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Value1");

        assertEquals("ArrayBinaryTree.testRight()", right, tree.right(root));
        assertEquals("ArrayBinaryTree.testRight()", null, tree.right(right));
    }

    private static void testLeft() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> left = tree.addLeft(root, "Value1");

        assertEquals("ArrayBinaryTree.testLeft()", left, tree.left(root));
        assertEquals("ArrayBinaryTree.testLeft()", null, tree.left(left));
    }

    private static void testParent() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Value1");
        Node<String> left = tree.addLeft(root, "Value2");

        assertEquals("ArrayBinaryTree.testParent()", root, tree.parent(right));
        assertEquals("ArrayBinaryTree.testParent()", root, tree.parent(left));
        assertEquals("ArrayBinaryTree.testParent()", null, tree.parent(root));
    }

    private static void testSize() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Value1");
        Node<String> right2 = tree.addRight(right, "Value2");
        Node<String> left = tree.addRight(right2, "Value3");
        assertEquals("ArrayBinaryTree.testSize()", 4, tree.size());
    }

    private static void testSet() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Value");
        String oldValue = tree.set(right, "NewValue");
        assertEquals("ArrayBinaryTree.testSet()", "Value", oldValue);
        assertEquals("ArrayBinaryTree.testSet()", "NewValue", tree.right(root).getElement());

    }

    private static void testAddRight() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Right");
        assertEquals("ArrayBinaryTree.testAddLeft()", right, tree.right(root));

        try {
            tree.addRight(root, "newR");
            fail("ArrayBinaryTree.testAdd() failed");
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTree.testAdd()", "This node already has right child", e.getMessage());
        }
    }

    private static void testAddLeft() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> left = tree.addLeft(root, "Left");
        assertEquals("ArrayBinaryTree.testAddLeft()", left, tree.left(root));

        try {
            tree.addLeft(root, "newLeft");
            fail("ArrayBinaryTree.testAdd() failed");
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTree.testAdd()", "This node already has left child", e.getMessage());
        }
    }

    private static void testRemove() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> right = tree.addRight(root, "Value1");
        Node<String> right2 = tree.addRight(right, "Value2");

        try {
            tree.remove(right);
            fail("ArrayBinaryTree.testRemove() failed");
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTree.testRemove()", "Node has children", e.getMessage());
        }

        String value = tree.remove(right2);
        assertEquals("ArrayBinaryTree.testRemove()", "Value2", value);
        assertEquals("ArrayBinaryTree.testRemove()", null, tree.right(right));
    }

    private static void testAdd() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        Node<String> left = tree.add(root, "Left");
        assertEquals("ArrayBinaryTree.testAdd()", left, tree.left(root));
        Node<String> right = tree.add(root, "Right");
        assertEquals("ArrayBinaryTree.testAdd()", right, tree.right(root));

        try {
            tree.add(root, "w");
            fail("ArrayBinaryTree.testAdd() failed");
        } catch (IllegalArgumentException e) {
            assertEquals("ArrayBinaryTree.testAdd()", "Node already has two children", e.getMessage());
        }
    }

    private static void testAddRoot() {
        BinaryTree<String> tree = new ArrayBinaryTree<>();
        Node<String> root = tree.addRoot("Root");
        assertEquals("ArrayBinaryTree.testAddRoot", "Root", root.getElement());
        assertEquals("ArrayBinaryTree.root()", root, tree.root());

        try {
            tree.addRoot("NewRoot");
            fail("ArrayBinaryTree.testAddRoot failed");
        } catch (IllegalStateException e) {
            assertEquals("ArrayBinaryTree.testAddRoot_again", "Tree already has root element", e.getMessage());
        }
    }
}
