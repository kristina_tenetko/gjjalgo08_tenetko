package com.getjavajob.training.algo08.tenetkok.lesson07.binary;

import com.getjavajob.training.algo08.tenetkok.lesson07.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {

    private ArrayList<Node<E>> array = new ArrayList<>();
    private int size;

    private int indexOfNode(Node<E> node) {
        return array.indexOf(node);
    }

    private int parentIndex(int index) {
        return index % 2 == 0 ? (index - 2) / 2 : (index - 1) / 2;
    }

    private int leftIndex(int index) {
        return 2 * index + 1;
    }

    private int rightIndex(int index) {
        return 2 * index + 2;
    }

    private void increaseArrayIfNeeded(int index) {
        if (array.size() == 0) {
            array.add(0, null);
        }
        if (index >= array.size()) {
            if (array.size() == 0) {
                array.add(0, null);
            } else {
                int arraySize = array.size();
                for (int i = 0; i <= index - arraySize + 1; i++) {
                    array.add(null);
                }
            }
        }
    }

    private Node<E> addElementToIndex(E e, int index) {
        increaseArrayIfNeeded(index + 1);
        NodeImpl<E> node = new NodeImpl<>(e);
        array.set(index, node);
        size++;
        return node;
    }

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        int nodeIndex = indexOfNode(p);
        if (nodeIndex == -1) {
            throw new IllegalArgumentException("There is no node");
        }
        int leftIndex = leftIndex(nodeIndex);
        return leftIndex < array.size() ? array.get(leftIndex) : null;
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        int nodeIndex = indexOfNode(p);
        if (nodeIndex == -1) {
            throw new IllegalArgumentException("There is no node");
        }
        int rightIndex = rightIndex(nodeIndex);
        return rightIndex < array.size() ? array.get(rightIndex) : null;
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        int nodeIndex = indexOfNode(n);
        int leftIndex = leftIndex(nodeIndex);
        if (nodeIndex == -1) {
            throw new IllegalArgumentException("There is no node");
        } else if (leftIndex < array.size() && array.get(leftIndex) != null) {
            throw new IllegalArgumentException("This node already has left child");
        }
        return addElementToIndex(e, leftIndex);
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        int nodeIndex = indexOfNode(n);
        int rightIndex = rightIndex(nodeIndex);
        if (nodeIndex == -1) {
            throw new IllegalArgumentException("There is no node");
        } else if (rightIndex < array.size() && array.get(rightIndex) != null) {
            throw new IllegalArgumentException("This node already has right child");
        }
        return addElementToIndex(e, rightIndex);
    }

    @Override
    public Node<E> root() {
        return (array.size() > 0) ? array.get(0) : null;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        int index = array.indexOf(n);
        if (index == -1) {
            throw new IllegalArgumentException("There is no node");
        }
        return index == 0 ? null : array.get(parentIndex(index));
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root() != null) {
            throw new IllegalStateException("Tree already has root element");
        }
        return addElementToIndex(e, 0);
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (left(n) == null) {
            return addLeft(n, e);
        } else if (right(n) == null) {
            return addRight(n, e);
        } else {
            throw new IllegalArgumentException("Node already has two children");
        }
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        int index = array.indexOf(n);
        if (index == -1) {
            throw new IllegalArgumentException("There is no node");
        }
        E oldValue = n.getElement();
        array.set(index, new NodeImpl<>(e));
        return oldValue;
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        int index = indexOfNode(n);
        if (index == -1) {
            throw new IllegalArgumentException("There is no node");
        }
        if (this.right(n) != null || this.left(n) != null) {
            throw new IllegalArgumentException("Node has children");
        }
        size--;
        return array.set(index, null).getElement();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private int count;
            private int index;

            @Override
            public boolean hasNext() {
                return count < size;
            }

            @Override
            public E next() {
                for (; index < array.size(); index++) {
                    if (array.get(index) != null) {
                        count++;
                        return array.get(index).getElement();
                    }
                }
                return null;
            }
        };
    }

    @Override
    public Collection<Node<E>> nodes() {
        Collection<Node<E>> nodes = new ArrayList<>();
        for (Node node : array) {
            if (node != null) {
                nodes.add(node);
            }
        }
        return nodes;
    }

    protected static class NodeImpl<E> implements Node<E> {

        E value;

        public NodeImpl(E value) {
            this.value = value;
        }

        @Override
        public E getElement() {
            return value;
        }
    }
}