package com.getjavajob.training.algo08.tenetkok.lesson10;

import java.util.Arrays;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class BubbleSortTest {
    public static void main(String[] args) {
        testSort();
    }

    private static void testSort() {
        BubbleSort<Integer> bubble = new BubbleSort<>();
        Integer[] array = {3, 5, 6, 4, 1, 2};
        bubble.sorting(array);
        assertEquals("BubbleSortTest.testSort", "[1, 2, 3, 4, 5, 6]", Arrays.toString(array));
    }
}
