package com.getjavajob.training.algo08.tenetkok.lesson10;

import java.util.Arrays;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class QuickSortTest {
    public static void main(String[] args) {
        testSort();
    }

    private static void testSort() {
        QuickSort<Integer> sort = new QuickSort<>();
        Integer[] array = {3, 5, 6, 4, 1, 2};
        sort.sorting(array);
        assertEquals("QuickSortTest.testSort", "[1, 2, 3, 4, 5, 6]", Arrays.toString(array));
    }
}
