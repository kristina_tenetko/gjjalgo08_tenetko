package com.getjavajob.training.algo08.tenetkok.lesson10;

import java.util.Arrays;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class InsertionSortTest {
    public static void main(String[] args) {
        testSort();
    }

    private static void testSort() {
        InsertionSort<Integer> sort = new InsertionSort<>();
        Integer[] array = {3, 5, 6, 4, 1, 2};
        sort.sorting(array);
        assertEquals("InsertionSortTest.testSort", "[1, 2, 3, 4, 5, 6]", Arrays.toString(array));
    }
}
