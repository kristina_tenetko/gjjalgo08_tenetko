package com.getjavajob.training.algo08.tenetkok.lesson10;

public class InsertionSort<E extends Comparable<E>> {
    public void sorting(E[] arr) {
        for (int i = 1; i < arr.length; i++) {
            E x = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j].compareTo(x) > 0) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = x;
        }
    }
}
