package com.getjavajob.training.algo08.tenetkok.lesson10;

public class BubbleSort<E extends Comparable<E>> {
    public void sorting(E[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j].compareTo(arr[j + 1]) > 0) {
                    E buff = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = buff;
                }
            }
        }
    }
}
