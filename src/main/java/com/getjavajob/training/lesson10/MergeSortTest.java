package com.getjavajob.training.algo08.tenetkok.lesson10;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class MergeSortTest {
    public static void main(String[] args) {
        testSort();
        testSortMaxInt();
    }

    private static void testSort() {
        MergeSort<Integer> bubble = new MergeSort<>();
        Integer[] array = {3, 5, 6, 4, 1, 2};
        bubble.sorting(array);
        assertEquals("MergeSortTest.testSort", "[1, 2, 3, 4, 5, 6]", Arrays.toString(array));
    }

    private static void testSortMaxInt() {

        Integer size = Integer.MAX_VALUE / 100;
        Integer[] array = new Integer[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = random.nextInt();
        }
        MergeSort<Integer> sort = new MergeSort<>();
        List<Integer> list = Arrays.asList(array);
        sort.sorting(array);
        Collections.sort(list);
        List<Integer> result = Arrays.asList(array);
        assertEquals("MergeSortTest.testSortMaxInt", list, result);
    }

}
