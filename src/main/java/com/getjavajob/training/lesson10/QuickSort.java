package com.getjavajob.training.algo08.tenetkok.lesson10;

public class QuickSort<E extends Comparable<E>> {
    public void sorting(E[] arr) {
        quickSort(arr, 0, arr.length - 1);
    }

    private int partition(E arr[], int left, int right) {
        int i = left, j = right;
        E tmp;
        E pivot = arr[(left + right) / 2];

        while (i <= j) {
            while (arr[i].compareTo(pivot) < 0) {
                i++;
            }
            while (arr[j].compareTo(pivot) > 0) {
                j--;
            }
            if (i <= j) {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
        }
        return i;
    }

    private void quickSort(E arr[], int left, int right) {
        int index = partition(arr, left, right);
        if (left < index - 1) {
            quickSort(arr, left, index - 1);
        }
        if (index < right) {
            quickSort(arr, index, right);
        }
    }
}
