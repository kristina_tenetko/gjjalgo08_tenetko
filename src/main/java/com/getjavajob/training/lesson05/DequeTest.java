package com.getjavajob.training.algo08.tenetkok.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NoSuchElementException;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class DequeTest {

    public static void main(String[] args) {
        testAddFirst();
        testAddLast();
        testOfferFirst();
        testOfferLast();
        testRemoveFirst();
        testRemoveLast();
        testPollFirst();
        testPollLast();
        testGetFirst();
        testGetLast();
        testPeekFirst();
        testPeekLast();
        testRemoveFirstOccurrence();
        testRemoveLastOccurrence();
    }

    public static void testAddFirst() {
        Deque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.addFirst(1);
        Object[] result = {1, 0};
        assertEquals("DequeTest.testAddFirst", result, deque.toArray());

        String msg = "Test failed";
        try {
            deque.addFirst(null);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DequeTest.testAddFirstFail", "NullPointerException", e.getClass().getSimpleName());
        }
    }

    public static void testAddLast() {
        Deque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.addLast(1);
        Object[] result = {0, 1};
        assertEquals("DequeTest.testAddLast", result, deque.toArray());

        String msg = "Test failed";
        try {
            deque.addLast(null);
            fail(msg);
        } catch (Exception e) {
            assertEquals("DequeTest.testAddLastFail", "NullPointerException", e.getClass().getSimpleName());
        }
    }

    public static void testOfferFirst() {
        Deque<Integer> deque = new ArrayDeque();
        deque.offer(0);

        assertEquals("DequeTest.testOfferFirst1", true, deque.offerFirst(1));
        Object[] result = {1, 0};
        assertEquals("DequeTest.testOfferFirst2", result, deque.toArray());
    }

    public static void testOfferLast() {
        Deque<Integer> deque = new ArrayDeque();
        deque.offer(0);
        assertEquals("DequeTest.testOfferLast1", true, deque.offerLast(1));
        Object[] result = {0, 1};
        assertEquals("DequeTest.testOfferLast2", result, deque.toArray());
    }

    private static void testRemoveFirst() {
        Deque<Integer> deque = new ArrayDeque();
        String msg = "Test failed";
        try {
            deque.removeFirst();
            fail(msg);
        } catch (Exception e) {
            assertEquals("DequeTest.testRemoveFirstFail", "NoSuchElementException", e.getClass().getSimpleName());
        }
        deque.add(0);
        deque.add(1);

        assertEquals("DequeTest.testRemoveFirst", 0, deque.removeFirst());

    }

    private static void testRemoveLast() {
        Deque<Integer> deque = new ArrayDeque();
        String msg = "Test failed";
        try {
            deque.removeLast();
            fail(msg);
        } catch (Exception e) {
            assertEquals("DequeTest.testRemoveLastException", "NoSuchElementException", e.getClass().getSimpleName());
        }
        deque.add(0);
        deque.add(1);

        assertEquals("DequeTest.testRemoveLast", 1, deque.removeLast());

    }

    private static void testPollFirst() {
        Deque<Integer> deque = new ArrayDeque();
        String msg = "Test failed";
        try {
            int i = deque.pollFirst();
            fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.testPollFirstFail", "NullPointerException", e.getClass().getSimpleName());
        }
        deque.add(0);
        deque.add(1);

        assertEquals("DequeTest.testPollFirst", 0, deque.pollFirst());
    }

    private static void testPollLast() {
        Deque<Integer> deque = new ArrayDeque();
        String msg = "Test failed";
        try {
            int i = deque.pollLast();
            fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.testPollLastFail", "NullPointerException", e.getClass().getSimpleName());
        }
        deque.add(0);
        deque.add(1);

        assertEquals("DequeTest.testPollLast", 1, deque.pollLast());
    }


    private static void testGetFirst() {
        Deque<Integer> deque = new ArrayDeque();
        String msg = "Test failed";
        try {
            deque.getFirst();
            fail(msg);
        } catch (Exception e) {
            assertEquals("DequeTest.testGetFirstFail", "NoSuchElementException", e.getClass().getSimpleName());
        }
        deque.add(0);
        deque.add(1);

        assertEquals("DequeTest.testGetFirst", 0, deque.getFirst());
    }

    private static void testGetLast() {
        Deque<Integer> deque = new ArrayDeque();
        String msg = "Test failed";
        try {
            deque.getLast();
            fail(msg);
        } catch (NoSuchElementException e) {
            assertEquals("DequeTest.testGetLastFail", "NoSuchElementException", e.getClass().getSimpleName());
        }
        deque.add(0);
        deque.add(1);

        assertEquals("DequeTest.testGetLast", 1, deque.getLast());

    }

    private static void testPeekFirst() {
        Deque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);

        assertEquals("DequeTest.testPeekFirst", 0, deque.peekFirst());
    }

    private static void testPeekLast() {
        Deque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);

        assertEquals("DequeTest.testPeekLast", 1, deque.peekLast());
    }

    private static void testRemoveFirstOccurrence() {
        Deque<Integer> deque = new ArrayDeque();
        deque.add(1);
        deque.add(0);
        deque.add(1);

        assertEquals("DequeTest.testRemoveFirstOccurrence1", true, deque.removeFirstOccurrence(1));
        Object[] result = {0, 1};
        assertEquals("DequeTest.testRemoveFirstOccurrence2", result, deque.toArray());
    }

    private static void testRemoveLastOccurrence() {
        Deque<Integer> deque = new ArrayDeque();
        deque.add(1);
        deque.add(0);
        deque.add(1);

        assertEquals("DequeTest.testRemoveLastOccurrence1", true, deque.removeLastOccurrence(1));
        Object[] result = {1, 0};
        assertEquals("DequeTest.testRemoveLastOccurrence2", result, deque.toArray());
    }
}
