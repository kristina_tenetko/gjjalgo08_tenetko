package com.getjavajob.training.algo08.tenetkok.lesson05;

import java.util.NoSuchElementException;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class LinkedListStackTest {

    public static void main(String[] args) {
        testPush();
        testPop();
    }

    public static void testPush() {
        LinkedListStack<Integer> stack = new LinkedListStack<>();
        for (int i = 0; i < 3; i++) {
            stack.push(i);
        }
        String result = "[2, 1, 0]";
        assertEquals("LinkedListStackTest.testPush", result, stack.asString());
    }

    public static void testPop() {
        LinkedListStack<Integer> stack = new LinkedListStack<>();
        String msg = "Test pop() failed";
        try {
            stack.pop();
            fail(msg);
        } catch (NoSuchElementException ex) {
            assertEquals("SinglyLinkedListTest.testPopFail", "NoSuchElementException", ex.getClass().getSimpleName());
        }
        for (int i = 0; i < 3; i++) {
            stack.push(i);
        }
        stack.pop();
        String result = "[1, 0]";
        assertEquals("LinkedListStackTest.testPop", result, stack.asString());
    }
}
