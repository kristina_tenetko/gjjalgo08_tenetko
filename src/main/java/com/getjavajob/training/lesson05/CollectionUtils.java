package com.getjavajob.training.algo08.tenetkok.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

interface Predicate<E> {
    boolean filter(E input);
}

interface Transformer<I, O> {
    O transform(I input);
}

interface NewLogic<E> {
    E applyLogic(E input);
}

public class CollectionUtils<E> {

    public static <E> boolean filter(Iterable<E> collection, Predicate<E> predicate) {
        boolean isModified = false;
        Iterator<E> it = collection.iterator();
        while (it.hasNext()) {
            if (!predicate.filter(it.next())) {
                it.remove();
                isModified = true;
            }
        }
        return isModified;
    }

    public static <I, O> Collection<O> transform(Collection<I> collection, Transformer<I, O> transformer) {
        List<O> newList = new ArrayList<>();
        Iterator<I> it = collection.iterator();
        while (it.hasNext()) {
            newList.add(transformer.transform(it.next()));
        }
        return newList;
    }

    public static <I, O> Collection<O> transformCollection(Collection<I> collection, Transformer<I, O> transformer) {
        Collection<O> newCollection = transform(collection, transformer);
        collection.clear();
        collection.addAll((Collection<I>) newCollection);
        return (Collection<O>) collection;
    }

    public static <E> void forAllDo(Collection<E> collection, NewLogic<E> logic) {
        Iterator<E> it = collection.iterator();
        while (it.hasNext()) {
            logic.applyLogic(it.next());
        }
    }

    public static <E> Collection<E> unmodifiableCollection(Collection<E> collection) {
        return new UnmodifiableCollection<>(collection);
    }

    public static class UnmodifiableCollection<E> implements Collection<E> {
        Collection<E> collection;

        public UnmodifiableCollection(Collection<E> collection) {
            if (collection == null) {
                throw new NullPointerException();
            }
            this.collection = collection;
        }

        @Override
        public int size() {
            return collection.size();
        }

        @Override
        public boolean isEmpty() {
            return collection.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            return collection.contains(o);
        }

        @Override
        public Iterator<E> iterator() {
            return new Iterator<E>() {
                private final Iterator<? extends E> iterator = collection.iterator();

                public boolean hasNext() {
                    return iterator.hasNext();
                }

                public E next() {
                    return iterator.next();
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

        @Override
        public Object[] toArray() {
            return collection.toArray();
        }

        @Override
        public <T> T[] toArray(T[] ts) {
            return collection.toArray(ts);
        }

        @Override
        public boolean add(E e) {
            throw new UnsupportedOperationException("Invalid operation for this collection");
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException("Invalid operation for this collection");
        }

        @Override
        public boolean containsAll(Collection<?> collection) {
            throw new UnsupportedOperationException("Invalid operation for this collection");
        }

        @Override
        public boolean addAll(Collection<? extends E> collection) {
            throw new UnsupportedOperationException("Invalid operation for this collection");
        }

        @Override
        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException("Invalid operation for this collection");
        }

        @Override
        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException("Invalid operation for this collection");
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException("Invalid operation for this collection");
        }
    }
}
