package com.getjavajob.training.algo08.tenetkok.lesson05;

public interface Stack<E> {
    void push(E e); // add element to the top

    E pop(); // removes element from the top
}

