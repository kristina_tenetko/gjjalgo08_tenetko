package com.getjavajob.training.algo08.tenetkok.lesson05;

import java.util.LinkedList;
import java.util.Queue;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class QueueTest {

    public static void main(String[] args) {
        testAdd();
        testOffer();
        testRemove();
        testPoll();
        testElement();
        testPeek();
    }

    public static void testAdd() {
        Queue<Integer> queue = new LinkedList<>();
        for (int i = 0; i < 3; i++) {
            queue.add(i);
        }
        Object[] result = {0, 1, 2};
        assertEquals("QueueTest.testAdd", result, queue.toArray());
    }

    public static void testOffer() {
        Queue<Integer> queue = new LinkedList<>();
        for (int i = 0; i < 3; i++) {
            queue.add(i);
        }
        queue.offer(3);
        Object[] result = {0, 1, 2, 3};
        assertEquals("QueueTest.testOffer", result, queue.toArray());
    }

    public static void testRemove() {
        Queue<Integer> queue = new LinkedList<>();
        String msg = "NoSuchElementException";
        try {
            queue.remove();
            fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.testRemoveFail", msg, e.getClass().getSimpleName());
        }
        for (int i = 0; i < 3; i++) {
            queue.add(i);
        }
        queue.remove();
        Object[] result = {1, 2};
        assertEquals("QueueTest.testRemove", result, queue.toArray());
    }


    public static void testPoll() {
        Queue<Integer> queue = new LinkedList<>();
        String msg = "NullPointerException";
        try {
            int i = queue.poll();
            fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.testPollFail", msg, e.getClass().getSimpleName());
        }
        for (int i = 0; i < 3; i++) {
            queue.add(i);
        }
        queue.poll();
        Object[] result = {1, 2};
        assertEquals("QueueTest.testPoll", result, queue.toArray());
    }


    public static void testElement() {
        Queue<Integer> queue = new LinkedList<>();
        String msg = "NoSuchElementException";
        try {
            int k = queue.element();
            fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.testElement", msg, e.getClass().getSimpleName());
        }
        for (int i = 0; i < 3; i++) {
            queue.add(i);
        }
        Object head = queue.element();
        assertEquals("QueueTest.testElement", head, 0);
    }

    public static void testPeek() {
        Queue<Integer> queue = new LinkedList<>();
        String msg = "NullPointerException";
        try {
            int k = queue.peek();
            fail(msg);
        } catch (Exception e) {
            assertEquals("QueueTest.testPeekFail", msg, e.getClass().getSimpleName());
        }
    }
}
