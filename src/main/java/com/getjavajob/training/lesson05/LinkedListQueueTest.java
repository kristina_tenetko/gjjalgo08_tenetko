package com.getjavajob.training.algo08.tenetkok.lesson05;

import java.util.NoSuchElementException;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class LinkedListQueueTest {

    public static void main(String[] args) {
        testAdd();
        testRemove();
    }

    public static void testAdd() {
        LinkedListQueue<Integer> linkListQueue = new LinkedListQueue<>();
        for (int i = 0; i < 3; i++) {
            linkListQueue.add(i);
        }
        String result = "0 1 2 ";
        assertEquals("LinkedListQueueTest.testAdd", result, linkListQueue.asString());
    }

    public static void testRemove() {
        LinkedListQueue<Integer> linkListQueue = new LinkedListQueue<>();
        String msg = "Test remove() failed";
        try {
            linkListQueue.remove();
            fail(msg);
        } catch (NoSuchElementException ex) {
            assertEquals("SinglyLinkedListTest.testPopFail", "NoSuchElementException", ex.getClass().getSimpleName());
        }
        for (int i = 0; i < 3; i++) {
            linkListQueue.add(i);
        }
        linkListQueue.remove();
        String result = "1 2 ";
        assertEquals("LinkedListQueueTest.testRemove", result, linkListQueue.asString());
    }
}
