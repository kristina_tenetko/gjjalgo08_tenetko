package com.getjavajob.training.algo08.tenetkok.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.getjavajob.training.algo08.tenetkok.lesson05.CollectionUtils.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class CollectionUtilsTest {

    public static void main(String args[]) {
        testFilter();
        testTranform();
        testTranformCollection();
        testForAllDo();
        testUnmodifiableCollection();
    }

    public static void testFilter() {
        Employee employee1 = new Employee("Ivanov", "Dima", 20000);
        Employee employee2 = new Employee("Ivanova", "Masha", 30000);
        Employee employee3 = new Employee("Petrov", "Ivan", 40000);
        List<Employee> list = new ArrayList<>();
        list.add(employee1);
        list.add(employee2);
        list.add(employee3);
        Predicate<Employee> predicate = new Predicate<Employee>() {
            @Override
            public boolean filter(Employee emp) {
                return emp.getFamilyName().toLowerCase().contains("ivanov");
            }
        };
        filter(list, predicate);
        List<Employee> result = new ArrayList<>();
        result.add(employee1);
        result.add(employee2);
        assertEquals("CollectionUtilsTest.testFilter", result, list);
    }

    public static void testTranform() {
        Employee employee1 = new Employee("Ivanov", "Dima", 20000);
        Employee employee2 = new Employee("Ivanova", "Masha", 30000);
        Employee employee3 = new Employee("Petrov", "Ivan", 40000);
        List<Employee> list = new ArrayList<>();
        list.add(employee1);
        list.add(employee2);
        list.add(employee3);
        Transformer<Employee, String> transformer = new Transformer<Employee, String>() {
            @Override
            public String transform(Employee input) {
                return input.getFamilyName();
            }
        };
        List<String> result = new ArrayList<>();
        result.add(employee1.getFamilyName());
        result.add(employee2.getFamilyName());
        result.add(employee3.getFamilyName());
        assertEquals("CollectionUtilsTest.tesTransform", result, transform(list, transformer));
    }

    public static void testTranformCollection() {
        Employee employee1 = new Employee("Ivanov", "Dima", 20000);
        Employee employee2 = new Employee("Ivanova", "Masha", 30000);
        Employee employee3 = new Employee("Petrov", "Ivan", 40000);
        List<Employee> list = new ArrayList<>();
        list.add(employee1);
        list.add(employee2);
        list.add(employee3);
        Transformer<Employee, String> transformer = new Transformer<Employee, String>() {
            @Override
            public String transform(Employee input) {
                return input.getFamilyName();
            }
        };
        List<String> result = new ArrayList<>();
        result.add(employee1.getFamilyName());
        result.add(employee2.getFamilyName());
        result.add(employee3.getFamilyName());
        transformCollection(list, transformer);
        assertEquals("CollectionUtilsTest.testTransformCollection", result, list);
    }

    public static void testForAllDo() {
        Employee employee1 = new Employee("Ivanov", "Dima", 20000);
        Employee employee2 = new Employee("Ivanova", "Masha", 30000);
        Employee employee3 = new Employee("Petrov", "Ivan", 40000);
        List<Employee> list = new ArrayList<>();
        list.add(employee1);
        list.add(employee2);
        list.add(employee3);
        NewLogic<Employee> logic = new NewLogic<Employee>() {
            @Override
            public Employee applyLogic(Employee input) {
                input.setSalary(input.getSalary() * 3 / 2);
                return input;
            }
        };
        forAllDo(list, logic);
        List<Employee> result = new ArrayList<>();
        employee1.setSalary(employee1.getSalary() * 3 / 2);
        employee2.setSalary(employee2.getSalary() * 3 / 2);
        employee3.setSalary(employee3.getSalary() * 3 / 2);
        result.add(employee1);
        result.add(employee2);
        result.add(employee3);
        assertEquals("CollectionUtilsTest.testForAllDo", result, list);
    }

    public static void testUnmodifiableCollection() {
        Employee employee1 = new Employee("Ivanov", "Dima", 20000);
        Employee employee2 = new Employee("Ivanova", "Masha", 30000);
        List<Employee> list = new ArrayList<>();
        list.add(employee1);
        Collection<Employee> newCollection = unmodifiableCollection(list);

        String msg = "testUnmodifiableCollection failed";
        try {
            newCollection.add(employee2);
            fail(msg);
        } catch (UnsupportedOperationException e) {
            assertEquals("CollectionUtilsTest.testUnmodifiableCollection", "UnsupportedOperationException", e.getClass().getSimpleName());
        }
    }

    static class Employee {
        private String familyName;
        private String firstName;
        private int salary;

        public Employee(String familyName, String firstName, int salary) {
            this.familyName = familyName;
            this.firstName = firstName;
            this.salary = salary;
        }

        public int getSalary() {
            return salary;
        }

        public void setSalary(int salary) {
            this.salary = salary;
        }

        public String getFamilyName() {
            return familyName;
        }

        public void setFamilyName(String familyName) {
            this.familyName = familyName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String employeeToString() {
            return getFamilyName() + " " + getFirstName();
        }
    }
}
