package com.getjavajob.training.algo08.tenetkok.lesson05;

import java.util.Arrays;
import java.util.List;

public class LinkedListStack<E> implements Stack<E> {

    private SinglyLinkedList<E> singlyLinkedList = new SinglyLinkedList<>();

    @Override
    public void push(E e) {
        singlyLinkedList.addAtBeginning(e);
    }

    @Override
    public E pop() {
        return singlyLinkedList.removeFirst();
    }

    public String asString() {
        List<E> list = singlyLinkedList.asList();
        if (list.size() == 0) {
            return "Stack is empty";
        }
        return Arrays.toString(list.toArray());
    }
}
