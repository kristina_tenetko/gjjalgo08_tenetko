package com.getjavajob.training.algo08.tenetkok.lesson05;

import java.util.Stack;

public class ExpressionCalculator {

    private static boolean isDouble(String str) {
        try {
            Double value = Double.parseDouble(str);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    private static boolean isOperator(char i) {
        return getPriority(i) > 0;
    }

    private static int getPriority(char i) {

        if (i == '(' || i == ')') {
            return 1;
        } else if (i == '-' || i == '+') {
            return 2;
        } else if (i == '*' || i == '/') {
            return 3;
        } else {
            return 0;
        }
    }

    public double calculateExpression(String expr) {
        return calculatePostfixExpression(convertInfixToPostfix(expr));
    }

    public String convertInfixToPostfix(String infix) {
        StringBuilder postfix = new StringBuilder();
        Stack<Character> operator = new Stack<>();
        char popped;

        for (int i = 0; i < infix.length(); i++) {
            char get = infix.charAt(i);
            if (!isOperator(get)) {
                if (!(postfix.length() > 0 && postfix.charAt(postfix.length() - 1) == ' ' && get == ' ')) {
                    postfix.append(get);
                }
            } else if (get == ')') {
                while ((popped = operator.pop()) != '(') {
                    if (postfix.length() > 0 && postfix.charAt(postfix.length() - 1) != ' ') {
                        postfix.append(' ');
                    }
                    postfix.append(popped);
                }
            } else {
                while (!operator.isEmpty() && get != '(' && getPriority(operator.peek()) >= getPriority(get)) {
                    if (postfix.length() > 0 && postfix.charAt(postfix.length() - 1) != ' ') {
                        postfix.append(' ');
                    }
                    postfix.append(operator.pop());
                }
                operator.push(get);
            }
        }
        while (!operator.isEmpty()) {
            if (postfix.length() > 0 && postfix.charAt(postfix.length() - 1) != ' ') {
                postfix.append(' ');
            }
            postfix.append(operator.pop());
        }
        return postfix.toString();
    }

    public double calculatePostfixExpression(String postfix) {
        Stack<Double> stack = new Stack<>();
        for (String token : postfix.split(" ")) {
            if (isOperator(token.charAt(0))) {
                stack.push(performOperation(token, stack.pop(), stack.pop()));
            } else if (isDouble(token)) {
                stack.push(Double.parseDouble(token));
            }
        }
        return stack.pop();
    }

    private double performOperation(String operation, double e1, double e2) {
        switch (operation) {
            case "+":
                return e1 + e2;
            case "-":
                return e2 - e1;
            case "*":
                return e1 * e2;
            case "/":
                return e2 / e1;
            default:
                throw new IllegalArgumentException("Invalid operator");
        }
    }
}
