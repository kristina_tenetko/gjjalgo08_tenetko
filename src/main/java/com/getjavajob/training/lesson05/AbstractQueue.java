package com.getjavajob.training.algo08.tenetkok.lesson05;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

abstract class AbstractQueue<E> implements Queue<E> {
    @Override
    public abstract boolean add(E e);

    @Override
    public abstract E remove();

    @Override
    public int size() {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public boolean contains(Object o) {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @SuppressWarnings("unchecked")
    @Override
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object[] toArray(Object[] objects) {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public boolean containsAll(Collection collection) {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public boolean offer(Object o) {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }


    @Override
    public E poll() {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public E element() {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }

    @Override
    public E peek() {
        throw new UnsupportedOperationException("Invalid operation for this queue");
    }
}
