package com.getjavajob.training.algo08.tenetkok.lesson05;

import java.util.NoSuchElementException;

public class LinkedListQueue<E> extends AbstractQueue<E> {

    private Node<E> first;
    private Node<E> last;

    @Override
    public boolean add(E e) {
        Node<E> oldLast = last;
        last = new Node<>();
        last.val = e;
        last.next = null;
        if (first == null) {
            first = last;
        } else {
            oldLast.next = last;
        }
        return true;
    }

    @Override
    public E remove() {
        if (first == null) {
            throw new NoSuchElementException("Queue is empty");
        }
        E removed = first.val;
        first = first.next;
        if (first == null) {
            last = null;
        }
        return removed;
    }

    public String asString() {
        if (first == null) {
            return "Queue is empty";
        }
        StringBuilder builder = new StringBuilder();
        Node<E> node = first;
        while (node != null) {
            builder.append(node.val).append(' ');
            node = node.next;
        }
        return builder.toString();
    }

    static class Node<E> {
        public Node<E> next;
        public E val;
    }
}


