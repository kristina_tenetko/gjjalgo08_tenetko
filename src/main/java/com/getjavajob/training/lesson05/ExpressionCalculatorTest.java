package com.getjavajob.training.algo08.tenetkok.lesson05;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class ExpressionCalculatorTest {

    public static void main(String args[]) {
        testConvertInfixToPostfix();
        testCalculatePostfixExpression();
        testCalculate();
    }

    public static void testConvertInfixToPostfix() {
        ExpressionCalculator calc = new ExpressionCalculator();
        String infix = "3 * (2 + 5)";
        String result = "3 2 5 + *";
        assertEquals("ExpressionCalculatorTest.testConvertInfixToPostfix1", result, calc.convertInfixToPostfix(infix));
        infix = "((((1 * (2 + 3)) - 3) + 4) * 5)";
        result = "1 2 3 + * 3 - 4 + 5 *";
        assertEquals("ExpressionCalculatorTest.testConvertInfixToPostfix2", result, calc.convertInfixToPostfix(infix));
    }

    public static void testCalculatePostfixExpression() {
        ExpressionCalculator calc = new ExpressionCalculator();
        String postfix = "4 14 7 / 2 * +";
        assertEquals("ExpressionCalculatorTest.testCalculatePostfixExpression", 8.0, calc.calculatePostfixExpression(postfix));
    }

    public static void testCalculate() {
        ExpressionCalculator calc = new ExpressionCalculator();
        String infix = "(4 + 8) * (6 - 5) / ((3 - 2) * (2 + 2))";
        assertEquals("ExpressionCalculatorTest.testCalculate", 3.0, calc.calculateExpression(infix));
    }
}
