package com.getjavajob.training.algo08.util;

import java.util.Arrays;

public class Assert {
    public static void assertEquals(String testName, Object[] expected, Object[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, Object expected, Object actual) {
        if (expected == null) {
            if (actual == null) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected null" + ", actual " + actual);
            }
        } else if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void fail(String mgs) {
        throw new AssertionError(mgs);
    }
}

