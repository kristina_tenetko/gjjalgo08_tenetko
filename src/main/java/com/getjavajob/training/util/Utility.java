package com.getjavajob.training.algo08.util;

import java.util.Scanner;

public class Utility {
    public static int readInt(final String message) {
        System.out.print(message);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    public static double readDouble(final String message) {
        System.out.print(message);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }

    public static String readStr(final String message) {
        System.out.print(message);
        Scanner in = new Scanner(System.in);
        return in.nextLine();
    }

    public static void printArray(Object[] array) {
        for (Object anArray : array) {
            System.out.print(anArray + " ");
        }
        System.out.println();
    }

    public static void printMultidimensionalArray(Object[][] array) {
        for (Object[] anArray : array) {
            for (int j = 0; j < anArray.length; j++) {
                System.out.printf("%4d", anArray[j]);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static int[] createArray() {
        int n = readInt("Enter amount of elements in the array: ");
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = readInt("Enter " + i + " element of the array: ");
        }
        return array;
    }

    public static int getNumberOfDigits(int number) {
        return number < 10 ? 1 : 1 + getNumberOfDigits(number / 10);
    }
}
