package com.getjavajob.training.algo08.util;

import static java.lang.System.currentTimeMillis;

public class StopWatch {
    public static long startTime;
    public static long endTime;

    public static long start() {
        startTime = currentTimeMillis();
        return startTime;
    }

    public static long getElapsedTime() {
        endTime = currentTimeMillis();
        return endTime - startTime;
    }
}
