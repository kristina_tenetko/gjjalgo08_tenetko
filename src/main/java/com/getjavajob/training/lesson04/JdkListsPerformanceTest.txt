--------Addition to the beginning--------
ArrayList.add(e): 2984 ms
LinkedList.add(e): 50 ms
--------Addition to the middle--------
ArrayList.add(e): 958 ms
LinkedList.add(e): 27334 ms
--------Addition to the end--------
ArrayList.add(e): 729 ms
LinkedList.add(e): 2641 ms
--------Remove from the beginning--------
ArrayList.remove(e): 1490 ms
LinkedList.remove(e): 22 ms
--------Remove from the middle--------
ArrayList.remove(e): 841 ms
LinkedList.remove(e): 16048 ms
--------Remove from the end--------
ArrayList.remove(e): 13 ms
LinkedList.remove(e): 72 ms