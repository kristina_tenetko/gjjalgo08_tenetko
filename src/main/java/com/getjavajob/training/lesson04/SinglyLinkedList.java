package com.getjavajob.training.algo08.tenetkok.lesson04;

import java.util.ArrayList;
import java.util.List;

public class SinglyLinkedList<E> {

    private Node<E> head;

    public void add(E e) {
        Node<E> newNode = new Node();
        newNode.val = e;
        Node<E> node = head;
        if (head == null) {
            head = newNode;
        } else {
            while (node.next != null) {
                node = node.next;
            }
            node.next = newNode;
        }
    }

    public E get(int index) {
        checkIndexOutOfBounds(index);
        Node<E> node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node.val;
    }

    public List<E> asList() {
        List<E> list = new ArrayList<>();
        Node<E> node = head;
        while (node != null) {
            list.add(node.val);
            node = node.next;
        }
        return list;
    }

    public int size() {
        Node<E> node = head;
        int size = 0;
        while (node != null) {
            size++;
            node = node.next;
        }
        return size;
    }

    public void reverse() {
        int size = size();
        for (int i = 0; i < size / 2; i++) {
            swap(i, size - 1 - i);
        }
    }

    private Node<E> getElementByIndex(int index) {
        checkIndexOutOfBounds(index);
        Node<E> node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    public void swap(int index1, int index2) {
        checkIndexOutOfBounds(index1);
        checkIndexOutOfBounds(index2);
        if (index1 != index2) {
            if (index1 > index2) {
                int tmp = index1;
                index1 = index2;
                index2 = tmp;
            }

            if (index1 == 0) {
                Node<E> node1 = head;
                Node<E> node2 = getElementByIndex(index2);
                Node<E> node2next = node2.next;

                if (index2 == 1) {
                    node2.next = node1;
                    node1.next = node2next;
                } else {
                    Node<E> node2prev = getElementByIndex(index2 - 1);
                    node2prev.next = node1;
                    node2.next = node1.next;
                    node1.next = node2next;

                }
                head = node2;

            } else {
                Node<E> node1prev = getElementByIndex(index1 - 1);
                Node<E> node1 = node1prev.next;

                if (index2 - index1 == 1) {
                    Node<E> node2 = node1.next;
                    node1prev.next = node2;
                    node1.next = node2.next;
                    node2.next = node1;
                }

                if (index2 - index1 > 1) {
                    Node<E> node2prev = getElementByIndex(index2 - 1);
                    Node<E> node2 = node2prev.next;
                    node1prev.next = node2;
                    node2prev.next = node1;
                    Node<E> node1next = node1.next;
                    node1.next = node2.next;
                    node2.next = node1next;
                }
            }
        }
    }

    private void checkIndexOutOfBounds(int index) {
        if (index >= size()) {
            throw new ArrayIndexOutOfBoundsException("Illegal index: " + index);
        }
    }

    private static class Node<E> {
        Node<E> next;
        E val;
    }
}
