package com.getjavajob.training.algo08.tenetkok.lesson04;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class CollectionTest {

    public static void main(String[] args) {
        testAdd();
        testAddAll();
        testClear();
        testContains();
        testContainsAll();
        testEquals();
        testHashCode();
        testIsEmpty();
        testIterator();
        testRemove();
        testRemoveAll();
        testRetainAll();
        testSize();
        testToArray();
        testToArrayT();
    }

    public static void testAdd() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        Object[] result = {0, 1};
        assertEquals("CollectionTest.testAdd(E e)", result, list.toArray());
    }

    public static void testAddAll() {
        List<Integer> list = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        list1.add(0);
        list1.add(1);
        list.addAll(list1);
        Object[] result = {0, 1};
        assertEquals("CollectionTest.testAddAll(Collection<? extends E> c)", result, list.toArray());
    }

    public static void testClear() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        list.clear();
        assertEquals("CollectionTest.testClear()", 0, list.size());
    }

    public static void testContains() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        assertEquals("CollectionTest.testContains(Object o)", true, list.contains(1));
    }

    public static void testContainsAll() {
        List<Integer> list = new ArrayList<>();
        List<Integer> newList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        for (int i = 0; i < 4; i++) {
            newList.add(i);
        }
        assertEquals("CollectionTest.testContainsAll(Collection<?> c)", true, newList.containsAll(list));
    }

    public static void testEquals() {
        List<Integer> list = new ArrayList<>();
        List<Integer> newList = new ArrayList<>();
        list.add(0);
        list.add(1);
        newList.add(0);
        newList.add(1);
        assertEquals("CollectionTest.testEquals(Object o)", true, list.equals(newList));
    }

    public static void testHashCode() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        assertEquals("CollectionTest.testHashcode()", 994, list.hashCode());
    }

    public static void testIsEmpty() {
        List<Integer> list = new ArrayList<>();
        assertEquals("CollectionTest.testIsEmpty()", true, list.isEmpty());
    }

    private static void testIterator() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        Iterator<Integer> it = list.iterator();
        Iterator<Integer> it1 = list.listIterator(1);
        assertEquals("CollectionTest.testIterator()", 1, it.next());
        assertEquals("CollectionTest.testIterator()", 2, it1.next());
    }

    public static void testRemove() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        assertEquals("CollectionTest.testRemove(Object o)", true, list.remove((Integer) 1));
        Object[] result = {0};
        assertEquals("CollectionTest.testRemove(Object o)", result, list.toArray());
    }

    public static void testRemoveAll() {
        List<Integer> list = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        for (int i = 0; i < 3; i++) {
            list1.add(i);
        }
        Object[] result = {3, 4};
        list.removeAll(list1);
        assertEquals("CollectionTest.testRemoveAll(Collection<?> c)", result, list.toArray());
    }

    public static void testRetainAll() {
        List<Integer> list = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        for (int i = 1; i < 4; i++) {
            list1.add(i);
        }
        Object[] result = {1, 2};
        list1.retainAll(list);
        assertEquals("CollectionTest.testRetainAll(Collection<?> c)", result, list1.toArray());
    }

    public static void testSize() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        assertEquals("CollectionTest.testSize()", 2, list.size());
    }

    public static void testToArray() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        Object[] result = {0, 1};
        assertEquals("CollectionTest.testToArray()", result, list.toArray());
    }

    public static void testToArrayT() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        Integer list1[] = new Integer[list.size()];
        list1 = list.toArray(list1);
        Object[] result = {0, 1};
        assertEquals("CollectionTest.testToArray(T[] a)", result, list1);
    }
}
