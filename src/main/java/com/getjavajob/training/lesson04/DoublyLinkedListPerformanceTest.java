package com.getjavajob.training.algo08.tenetkok.lesson04;

import java.util.LinkedList;
import java.util.List;

import static com.getjavajob.training.algo08.util.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.util.StopWatch.start;

public class DoublyLinkedListPerformanceTest {

    static final int NUMBER_OF_ELEMENTS = 50_000;

    public static void main(String[] args) {
        testAddToBeginDLL();
        testAddToBeginLL();
        testAddToMiddleDLL();
        testAddToMiddleLL();
        testAddToEndDLL();
        testAddToEndLL();
        testRemoveFromBeginningDLL();
        testRemoveFromBeginningLL();
        testRemoveFromMiddleDLL();
        testRemoveFromMiddleLL();
        testRemoveFromEndDLL();
        testRemoveFromEndLL();
    }

    public static void testAddToBeginDLL() {
        System.out.println("--------Addition to the beginning--------");
        DoublyLinkedList<Integer> intDLL = new DoublyLinkedList<>();
        intDLL.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            intDLL.add(0, i);
        }
        System.out.println("DoublyLinkedList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToBeginLL() {
        List<Integer> list = new LinkedList<>();
        list.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(0, i);
        }
        System.out.println("LinkedList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToMiddleDLL() {
        System.out.println("--------Addition to the middle--------");
        DoublyLinkedList<Integer> intDLL = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDLL.add(0);
        }
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            intDLL.add(intDLL.size() / 2, i);
        }
        System.out.println("DoublyLinkedList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToMiddleLL() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < 3; i++) {
            list.add(0);
        }
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(list.size() / 2, i);
        }
        System.out.println("LinkedList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToEndDLL() {
        System.out.println("--------Addition to the end--------");
        DoublyLinkedList<Integer> intDLL = new DoublyLinkedList<>();
        intDLL.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            intDLL.add(intDLL.size() - 1, i);
        }
        System.out.println("DoublyLinkedList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToEndLL() {
        List<Integer> list = new LinkedList<>();
        list.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(list.size() - 1, i);
        }
        System.out.println("LinkedList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromBeginningDLL() {
        System.out.println("--------Remove from the beginning--------");
        DoublyLinkedList<Integer> intDLL = new DoublyLinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            intDLL.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            intDLL.remove(0);
        }
        System.out.println("DoublyLinkedList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromBeginningLL() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(0);
        }
        System.out.println("LinkedList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromMiddleDLL() {
        System.out.println("--------Remove from the middle--------");
        DoublyLinkedList<Integer> intDLL = new DoublyLinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            intDLL.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            intDLL.remove(intDLL.size() / 2);
        }
        System.out.println("DoublyLinkedList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromMiddleLL() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(list.size() / 2);
        }
        System.out.println("LinkedList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromEndDLL() {
        System.out.println("--------Remove from the end--------");
        DoublyLinkedList<Integer> intDLL = new DoublyLinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            intDLL.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            intDLL.remove(intDLL.size() - 1);
        }
        System.out.println("DoublyLinkedList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromEndLL() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(list.size() - 1);
        }
        System.out.println("LinkedList.remove(e): " + getElapsedTime() + " ms");
    }
}
