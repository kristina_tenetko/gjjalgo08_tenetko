package com.getjavajob.training.algo08.tenetkok.lesson04;

import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<E> extends AbstractList<E> {

    private int size;
    private Element<E> first;
    private Element<E> last;
    private int modificationCount;

    public DoublyLinkedList() {
        size = 0;
        first = new Element<E>(null);
        last = new Element<E>(null);
        first.next = last;
        last.prev = first;
    }

    @Override
    public boolean add(E val) {
        if (isEmpty()) {
            Element<E> element = new Element<>(null, null, val);
            first = element;
            last = element;
        } else {
            Element<E> element = new Element<>(last, null, val);
            last.next = element;
            last = element;
        }
        size++;
        modificationCount++;
        return true;
    }

    @Override
    public void add(int index, E val) {
        checkIndexOutOfBoundsForAdd(index);
        modificationCount++;
        if (index == size) {
            Element<E> l = last;
            Element<E> newElement = new Element<>(l, null, val);
            last = newElement;
            if (l == null) {
                first = newElement;
            } else {
                l.next = newElement;
            }
        } else {
            Element<E> element = getElementByIndex(index);
            Element<E> pred = element.prev;
            Element<E> newElement = new Element<>(pred, element, val);
            element.prev = newElement;
            if (pred == null) {
                first = newElement;
            } else {
                pred.next = newElement;
            }
        }
        size++;
        modificationCount++;
    }

    private Element<E> getElementByIndex(int index) {
        checkIndexOutOfBounds(index);
        if (index < size / 2) {
            Element<E> element = first;
            for (int i = 0; i < index; i++) {
                element = element.next;
            }
            return element;
        } else {
            Element<E> element = last;
            for (int i = size - 1; i > index; i--) {
                element = element.prev;
            }
            return element;
        }
    }

    @Override
    public boolean remove(Object val) {
        int value = indexOf(val);
        if (value == -1) {
            return false;
        }
        remove(value);
        return true;
    }

    @Override
    public E get(int index) {
        checkIndexOutOfBounds(index);
        return getElementByIndex(index).val;
    }

    @Override
    public E set(int index, E val) {
        checkIndexOutOfBounds(index);
        Element<E> element = getElementByIndex(index);
        element.val = val;
        return (E) element;
    }

    @Override
    public E remove(int index) {
        return removeElement(getElementByIndex(index));
    }

    private E removeElement(Element<E> element) {
        E result = element.val;
        if (element == first) {
            first = element.next;
        } else if (element == last) {
            last = element.prev;
            element.prev.next = null;
            element.prev = element.next = null;
        } else {
            element.prev.next = element.next;
            element.next.prev = element.prev;
        }
        element.next = element.prev = null;
        size--;
        modificationCount++;
        return result;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object e) {
        return indexOf(e) != -1;
    }

    @Override
    public int indexOf(Object e) {
        if (isEmpty()) {
            throw new NoSuchElementException("List is empty");
        }
        Element<E> element = first;
        for (int i = 0; i < size; i++) {
            if (element.val.equals(e)) {
                return i;
            }
            element = element.next;
        }
        return -1;
    }

    private void checkIndexOutOfBoundsForAdd(int index) {
        if (index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Illegal index: " + index);
        }
    }

    private void checkIndexOutOfBounds(int index) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("Illegal index: " + index);
        }
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (Element<E> e = first; e != null; e = e.next) {
            result[i++] = e.val;
        }
        return result;
    }

    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl(0);
    }

    public ListIteratorImpl listIterator(int index) {
        checkIndexOutOfBoundsForAdd(index);
        return new ListIteratorImpl(index);
    }

    public class Element<V> {
        Element<V> next;
        Element<V> prev;
        V val;

        public Element(V val) {
            this.val = val;
        }

        public Element(Element<V> prev, Element<V> next, V val) {
            this.prev = prev;
            this.next = next;
            this.val = val;
        }
    }

    private class ListIteratorImpl implements ListIterator<E> {
        private Element<E> lastReturned;
        private Element<E> next;
        private int nextIndex;
        private int expectedModificationCount = modificationCount;

        ListIteratorImpl(int index) {
            if (index != size) {
                next = getElementByIndex(index);
            }
            nextIndex = index;
        }

        public boolean hasNext() {
            return nextIndex != size;
        }

        public E next() {
            checkConcurrentModification();
            if (!hasNext()) {
                throw new NoSuchElementException("Iterator bounds reached");
            }
            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.val;
        }

        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        public E previous() {
            checkConcurrentModification();
            if (!hasPrevious()) {
                throw new NoSuchElementException("Iterator bounds reached");
            }
            if (next == null) {
                next = last;
            } else {
                next = next.prev;
            }
            lastReturned = next;
            nextIndex--;
            return lastReturned.val;
        }

        public int nextIndex() {
            return nextIndex;
        }

        public int previousIndex() {
            return nextIndex - 1;
        }

        public void remove() {
            if (lastReturned == null) {
                throw new IllegalStateException("Cannot remove element");
            }
            checkConcurrentModification();

            next = lastReturned.next;
            removeElement(lastReturned);
            lastReturned = null;
            expectedModificationCount++;
        }

        public void set(E e) {
            if (lastReturned == null) {
                throw new IllegalStateException("Cannot set element");
            }
            checkConcurrentModification();
            lastReturned.val = e;
        }

        public void add(E e) {
            checkConcurrentModification();
            lastReturned = null;
            if (next == null) {
                Element<E> l = last;
                Element<E> newElement = new Element<>(l, null, e);
                last = newElement;
                if (l == null) {
                    first = newElement;
                } else {
                    l.next = newElement;
                }
            } else {
                Element<E> pred = next.prev;
                Element<E> newElement = new Element<>(pred, next, e);
                next.prev = newElement;
                if (pred == null) {
                    first = newElement;
                } else {
                    pred.next = newElement;
                }
            }
            size++;
            modificationCount++;
            nextIndex++;
            expectedModificationCount++;
        }

        final void checkConcurrentModification() {
            if (modificationCount != expectedModificationCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
