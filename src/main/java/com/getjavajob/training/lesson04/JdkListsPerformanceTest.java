package com.getjavajob.training.algo08.tenetkok.lesson04;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.getjavajob.training.algo08.util.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.util.StopWatch.start;

public class JdkListsPerformanceTest {

    static final int NUMBER_OF_ELEMENTS = 5_000_000;

    public static void main(String[] args) {
        testAddToBeginAL();
        testAddToBeginLL();
        testAddToMiddleAL();
        testAddToMiddleLL();
        testAddToEndAL();
        testAddToEndLL();
        testRemoveFromBeginningAL();
        testRemoveFromBeginningLL();
        testRemoveFromMiddleAL();
        testRemoveFromMiddleLL();
        testRemoveFromEndAL();
        testRemoveFromEndLL();
    }

    public static void testAddToBeginAL() {
        System.out.println("--------Addition to the beginning--------");
        List<Integer> list = new ArrayList<>();
        list.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(0, i);
        }
        System.out.println("ArrayList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToBeginLL() {
        List<Integer> list = new LinkedList<>();
        list.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(0, i);
        }
        System.out.println("LinkedList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToMiddleAL() {
        System.out.println("--------Addition to the middle--------");
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            list.add(0);
        }
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(list.size() / 2, i);
        }
        System.out.println("ArrayList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToMiddleLL() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < 3; i++) {
            list.add(0);
        }
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(list.size() / 2, i);
        }
        System.out.println("LinkedList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToEndAL() {
        System.out.println("--------Addition to the end--------");
        List<Integer> list = new ArrayList<>();
        list.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(list.size() - 1, i);
        }
        System.out.println("ArrayList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testAddToEndLL() {
        List<Integer> list = new LinkedList<>();
        list.add(0);
        start();
        for (int i = 1; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(list.size() - 1, i);
        }
        System.out.println("LinkedList.add(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromBeginningAL() {
        System.out.println("--------Remove from the beginning--------");
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(0);
        }
        System.out.println("ArrayList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromBeginningLL() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(0);
        }
        System.out.println("LinkedList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromMiddleAL() {
        System.out.println("--------Remove from the middle--------");
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(list.size() / 2);
        }
        System.out.println("ArrayList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromMiddleLL() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(list.size() / 2);
        }
        System.out.println("LinkedList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromEndAL() {
        System.out.println("--------Remove from the end--------");
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(list.size() - 1);
        }
        System.out.println("ArrayList.remove(e): " + getElapsedTime() + " ms");
    }

    public static void testRemoveFromEndLL() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(list.size() - 1);
        }
        System.out.println("LinkedList.remove(e): " + getElapsedTime() + " ms");
    }
}
