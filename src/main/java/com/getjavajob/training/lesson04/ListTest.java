package com.getjavajob.training.algo08.tenetkok.lesson04;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class ListTest {
    public static void main(String[] args) {
        testAdd();
        testAddAll();
        testGet();
        testLastIndexOf();
        testListIterator();
        testRemove();
        testSet();
        testSubList();
    }

    public static void testAdd() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        list.add(0, 3);
        list.add(3, 4);
        list.add(1, 5);
        Object[] result = {3, 5, 0, 1, 4, 2};
        assertEquals("ListTest.testAdd(int index, E element)", result, list.toArray());
    }

    public static void testAddAll() {
        List<Integer> list = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        for (int i = 3; i < 6; i++) {
            list1.add(i);
        }
        list.addAll(3, list1);
        Object[] result = {0, 1, 2, 3, 4, 5};
        assertEquals("ListTest.testAddAll(int index, Collection<? extends E> c)", result, list.toArray());
    }

    public static void testGet() {
        List<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        assertEquals("ListTest.testGet(int index)", 1, list.get(1));
    }

    private static void testLastIndexOf() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(1);
        assertEquals("ListTest.testLastIndexOf(Object o)", 1, list.lastIndexOf(1));
    }

    private static void testListIterator() {
        List<Integer> list = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        ListIterator listIterator = list.listIterator(list.size());
        while (listIterator.hasPrevious()) {
            builder.append(listIterator.previous()).append(' ');
        }
        String result = "2 1 0 ";
        assertEquals("ListTest.testListIterator(int index)", result, builder.toString());
    }

    public static void testRemove() {
        List<Integer> list = new ArrayList<>();
        for (int i = 3; i < 6; i++) {
            list.add(i);
        }
        list.remove(0);
        Object[] result = {4, 5};
        assertEquals("ListTest.testRemove(int index)", result, list.toArray());
    }

    public static void testSet() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            list.add(i);
        }
        list.set(1, 0);
        Object[] result = {0, 0, 2};
        assertEquals("ListTest.testSet(int index, E element)", result, list.toArray());
    }

    public static void testSubList() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        Object[] result = {0, 1, 2};
        assertEquals("ListTest.testSubList(int fromIndex, int toIndex)", result, list.subList(0, 3).toArray());
    }
}
