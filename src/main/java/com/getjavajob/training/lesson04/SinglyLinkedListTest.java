package com.getjavajob.training.algo08.tenetkok.lesson04;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class SinglyLinkedListTest {

    public static void main(String[] args) {
        testAdd();
        testGet();
        testSize();
        testReverse();
        testSwap();
    }

    public static void testAdd() {
        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList<>();
        singlyLinkedList.add(0);
        singlyLinkedList.add(1);
        Object[] result = {0, 1};
        assertEquals("SinglyLinkedListTest.testAdd", result, singlyLinkedList.asList().toArray());
    }

    public static void testGet() {
        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList<>();
        singlyLinkedList.add(0);
        singlyLinkedList.add(1);
        assertEquals("SinglyLinkedListTest.testGet", 0, singlyLinkedList.get(0));

        String msg = "Test get(int i) failed";
        try {
            singlyLinkedList.get(4);
            fail(msg);
        } catch (ArrayIndexOutOfBoundsException e1) {
            assertEquals("SinglyLinkedListTest.testGetFail", "Illegal index: 4", e1.getMessage());
        }
    }

    public static void testSize() {
        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList<>();
        singlyLinkedList.add(0);
        singlyLinkedList.add(1);
        assertEquals("SinglyLinkedListTest.testSize", 2, singlyLinkedList.size());
    }

    public static void testSwap() {
        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList<>();
        for (int i = 0; i < 5; i++) {
            singlyLinkedList.add(i);
        }

        singlyLinkedList.swap(0, 1);
        Object[] result = {1, 0, 2, 3, 4};
        assertEquals("SinglyLinkedListTest.testSwap1", result, singlyLinkedList.asList().toArray());

        singlyLinkedList.swap(0, 2);
        Object[] result1 = {2, 0, 1, 3, 4};
        assertEquals("SinglyLinkedListTest.testSwap2", result1, singlyLinkedList.asList().toArray());

        singlyLinkedList.swap(0, 4);
        Object[] result2 = {4, 0, 1, 3, 2};
        assertEquals("SinglyLinkedListTest.testSwap3", result2, singlyLinkedList.asList().toArray());

        singlyLinkedList.swap(1, 2);
        Object[] result3 = {4, 1, 0, 3, 2};
        assertEquals("SinglyLinkedListTest.testSwap3", result3, singlyLinkedList.asList().toArray());

        singlyLinkedList.swap(3, 1);
        Object[] result4 = {4, 3, 0, 1, 2};
        assertEquals("SinglyLinkedListTest.testSwap3", result4, singlyLinkedList.asList().toArray());

        String msg = "Test swap(int index1, int index2) failed";
        try {
            singlyLinkedList.swap(1, 5);
            fail(msg);
        } catch (ArrayIndexOutOfBoundsException e1) {
            assertEquals("SinglyLinkedListTest.testSwapFail", "Illegal index: 5", e1.getMessage());
        }
    }

    public static void testReverse() {
        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList<>();
        for (int i = 0; i < 5; i++) {
            singlyLinkedList.add(i);
        }
        Object[] result = {4, 3, 2, 1, 0};
        singlyLinkedList.reverse();

        assertEquals("SinglyLinkedListTest.testReverse", result, singlyLinkedList.asList().toArray());
    }
}
