package com.getjavajob.training.algo08.tenetkok.lesson04;

import java.util.*;
import java.util.function.UnaryOperator;

public abstract class AbstractList<E> implements List<E> {

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public void replaceAll(UnaryOperator<E> operator) {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public void sort(Comparator<? super E> c) {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public ListIterator<E> listIterator(int i) {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }

    @Override
    public Spliterator<E> spliterator() {
        throw new UnsupportedOperationException("Invalid operation for this list");
    }
}
