package com.getjavajob.training.algo08.tenetkok.lesson04;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DynamicArray<E> extends AbstractList<E> {

    private static final int DEFAULT_CAPACITY = 10;
    private E[] data;
    private int size;
    private int modificationCount;

    public DynamicArray(int capacity) {
        if (capacity < 0) {
            throw new ArrayIndexOutOfBoundsException("Illegal initial capacity, index < 0");
        }
        data = (E[]) new Object[capacity];
    }

    public DynamicArray() {
        this(DEFAULT_CAPACITY);
    }

    private void checkIndexOutOfBounds(int index) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("Illegal index: " + index);
        }
    }

    private void checkIndexOutOfBoundsForAdd(int index) {
        if (index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Illegal index: " + index);
        }
    }

    private void growIfFull() {
        if (size + 1 > data.length) {
            E[] bigger = (E[]) new Object[(int) (data.length * 1.5)];
            System.arraycopy(data, 0, bigger, 0, data.length);
            data = bigger;
        }
    }

    @Override
    public boolean add(E e) {
        growIfFull();
        data[size++] = e;
        modificationCount++;
        return true;
    }

    @Override
    public void add(int i, E e) {
        checkIndexOutOfBoundsForAdd(i);
        growIfFull();
        if (i != size) {
            System.arraycopy(data, i, data, i + 1, size - i);
        }
        data[i] = e;
        size++;
        modificationCount++;
    }

    @Override
    public E set(int i, E e) {
        checkIndexOutOfBounds(i);
        E oldValue = get(i);
        data[i] = e;
        return oldValue;
    }

    @Override
    public E get(int i) {
        checkIndexOutOfBounds(i);
        return data[i];
    }

    @Override
    public E remove(int i) {
        checkIndexOutOfBounds(i);
        E value = data[i];
        if (i < size - 1) {
            System.arraycopy(data, i + 1, data, i, size - i - 1);
        }
        data[--size] = null;
        modificationCount++;
        return value;
    }

    @Override
    public boolean remove(Object e) {
        int value = indexOf(e);
        if (value == -1) {
            return false;
        }
        remove(value);
        modificationCount++;
        return true;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int indexOf(Object e) {
        for (int i = 0; i < size; i++) {
            if (e == null && data[i] == null) {
                return i;
            }
            if (data[i].equals(e)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(Object e) {
        return indexOf(e) != -1;
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(data, size);
    }

    @Override
    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl(0);
    }

    @Override
    public ListIteratorImpl listIterator(int index) {
        checkIndexOutOfBounds(index);
        return new ListIteratorImpl(index);
    }

    private class ListIteratorImpl implements ListIterator<E> {
        int nextIndex;
        int lastReturned = -1;
        private int expectedModificationCount = modificationCount;

        public ListIteratorImpl(int index) {
            nextIndex = index;
        }

        @Override
        public boolean hasNext() {
            return nextIndex < size;
        }

        @Override
        public E next() {
            checkConcurrentModification();
            if (!hasNext()) {
                throw new NoSuchElementException("Iterator bounds reached");
            }
            Object[] array = DynamicArray.this.data;
            int i = nextIndex;
            if (i >= data.length) {
                throw new ConcurrentModificationException();
            } else {
                nextIndex++;
                lastReturned = i;
                return (E) array[lastReturned];
            }
        }

        @Override
        public boolean hasPrevious() {
            return nextIndex != 0;
        }

        public E previous() {
            checkConcurrentModification();
            int i = nextIndex - 1;
            if (i < 0) {
                throw new NoSuchElementException("Iterator bounds reached");
            } else {
                Object[] array = DynamicArray.this.data;
                if (i >= array.length) {
                    throw new ConcurrentModificationException();
                } else {
                    nextIndex = i;
                    lastReturned = i;
                    return (E) array[lastReturned = i];
                }
            }
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() {
            if (lastReturned < 0) {
                throw new IllegalStateException("Cannot remove element");
            }
            checkConcurrentModification();
            try {
                DynamicArray.this.remove(lastReturned);
                nextIndex = lastReturned;
                lastReturned = -1;
                expectedModificationCount = modificationCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void set(E e) {
            if (lastReturned < 0) {
                throw new IllegalStateException("Cannot set element");
            }
            checkConcurrentModification();
            try {
                DynamicArray.this.set(lastReturned, e);
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        @Override
        public void add(E e) {
            checkConcurrentModification();
            try {
                int i = nextIndex;
                DynamicArray.this.add(i, e);
                nextIndex = i + 1;
                lastReturned = -1;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        private void checkConcurrentModification() {
            if (modificationCount != expectedModificationCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
