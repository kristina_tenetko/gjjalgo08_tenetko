package com.getjavajob.training.algo08.tenetkok.lesson04;

import java.util.ConcurrentModificationException;
import java.util.ListIterator;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

public class DoublyLinkedListTest {

    public static void main(String[] args) {
        testAddObject();
        testAddIObject();
        testAddToBegin();
        testAddToMiddle();
        testAddToEnd();
        testSet();
        testGet();
        testRemoveObject();
        testRemoveIObject();
        testRemoveFromBeginning();
        testRemoveFromMiddle();
        testRemoveFromEnd();
        testIsEmpty();
        testSize();
        testContains();
        testIndexOf();
        testIteratorHasNext();
        testIteratorHasPrevious();
        testIteratorAdd();
        testIteratorRemove();
        testIteratorAdd();
        testConcurrentModificationException();
    }

    public static void testAddObject() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        Object[] result = {0, 1, 2};
        assertEquals("DoublyLinkedListTest.testAddObject", result, intDll.toArray());
    }

    public static void testAddIObject() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        intDll.add(1, 4);
        Object[] result = {0, 4, 1, 2};
        assertEquals("DoublyLinkedListTest.testAddIObject", result, intDll.toArray());

        String msg = "Test of add(int i, Object e) failed";
        try {
            intDll.add(5, 4);
            fail(msg);
        } catch (ArrayIndexOutOfBoundsException e1) {
            assertEquals("DoublyLinkedListTest.testAddIObjectFail", "Illegal index: 5", e1.getMessage());
        }
    }

    public static void testAddToBegin() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        intDll.add(0, 3);
        Object[] result = {3, 0, 1, 2};
        assertEquals("DoublyLinkedListTest.testAddToBegin", result, intDll.toArray());
    }

    public static void testAddToMiddle() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        intDll.add(intDll.size() / 2, 3);
        Object[] result = {0, 3, 1, 2};
        assertEquals("DoublyLinkedListTest.testAddToMiddle", result, intDll.toArray());
    }

    public static void testAddToEnd() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        intDll.add(intDll.size() - 1, 3);
        Object[] result = {0, 1, 3, 2};
        assertEquals("DoublyLinkedListTest.testAddToEnd", result, intDll.toArray());
    }


    public static void testSet() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        intDll.set(1, 0);
        Object[] result = {0, 0, 2};
        assertEquals("DoublyLinkedListTest.testSet", result, intDll.toArray());

        String msg = "Test set(int i, Object e) failed";
        try {
            intDll.set(4, 5);
            fail(msg);
        } catch (ArrayIndexOutOfBoundsException e1) {
            assertEquals("DoublyLinkedListTest.testSetFail", "Illegal index: 4", e1.getMessage());
        }
    }

    public static void testGet() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        assertEquals("DoublyLinkedListTest.testGet", 2, intDll.get(2));

        String msg = "Test get(int i) failed";
        try {
            intDll.get(4);
            fail(msg);
        } catch (ArrayIndexOutOfBoundsException e1) {
            assertEquals("DoublyLinkedListTest.testGetFail", "Illegal index: 4", e1.getMessage());
        }
    }

    public static void testRemoveObject() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        assertEquals("DoublyLinkedListTest.testRemoveObjectRemoved1", true, intDll.remove((Integer) 1));
        Object[] result = {0, 2};
        assertEquals("DoublyLinkedListTest.testRemoveObjectResult", result, intDll.toArray());
        assertEquals("DoublyLinkedListTest.testRemoveObjectRemoved2", false, intDll.remove((Integer) 6));
    }

    public static void testRemoveIObject() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }

        String msg = "Test remove(int i) failed";
        try {
            intDll.remove(3);
            fail(msg);
        } catch (ArrayIndexOutOfBoundsException e1) {
            assertEquals("DoublyLinkedListTest.testRemoveIObject", "Illegal index: 3", e1.getMessage());

        }
    }

    public static void testRemoveFromBeginning() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        intDll.remove(0);
        Object[] result = {1, 2};
        assertEquals("DoublyLinkedListTest.testRemoveFromBeginning", result, intDll.toArray());
    }

    public static void testRemoveFromMiddle() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        intDll.remove(intDll.size() / 2);
        Object[] result = {0, 2};
        assertEquals("DoublyLinkedListTest.testRemoveFromMiddle", result, intDll.toArray());
    }

    public static void testRemoveFromEnd() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        intDll.remove(intDll.size() - 1);
        Object[] result = {0, 1};
        assertEquals("DoublyLinkedListTest.testRemoveFromEnd", result, intDll.toArray());
    }

    public static void testIsEmpty() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        assertEquals("DoublyLinkedListTest.testIsEmpty1", intDll.isEmpty(), true);

        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        assertEquals("DoublyLinkedListTest.testIsEmpty2", intDll.isEmpty(), false);
    }

    public static void testSize() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        assertEquals("DoublyLinkedListTest.testSize", 3, intDll.size());
    }

    public static void testIndexOf() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        assertEquals("DoublyLinkedListTest.testIndexOfObject1", 2, intDll.indexOf(2));
        assertEquals("DoublyLinkedListTest.testIndexOfObject2", -1, intDll.indexOf(4));
    }

    public static void testContains() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        assertEquals("DoublyLinkedListTest.testContainsObject1", true, intDll.contains(2));
        assertEquals("DoublyLinkedListTest.testContainsObject2", false, intDll.contains(5));
    }

    public static void testIteratorHasNext() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 5; i++) {
            intDll.add(i);
        }
        ListIterator listIterator = intDll.listIterator(0);
        while (listIterator.hasNext()) {
            builder.append(listIterator.next()).append(' ');
        }
        String result = "0 1 2 3 4 ";
        assertEquals("DoublyLinkedListTest.testIteratorHasNext", result, builder.toString());
    }

    public static void testIteratorHasPrevious() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 5; i++) {
            intDll.add(i);
        }
        ListIterator listIterator = intDll.listIterator(intDll.size());
        while (listIterator.hasPrevious()) {
            builder.append(listIterator.previous()).append(' ');
        }
        String result = "4 3 2 1 0 ";
        assertEquals("DoublyLinkedListTest.testIteratorHasPrevious", result, builder.toString());
    }

    public static void testIteratorAdd() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        ListIterator listIterator = intDll.listIterator();
        for (int i = 0; i < 3; i++) {
            listIterator.add(i);
        }
        for (int i = 2; i >= 0; i--) {
            assertEquals("DoublyLinkedListTest.testIteratorAdd", i, listIterator.previous());
        }
    }

    public static void testIteratorRemove() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        ListIterator listIterator = intDll.listIterator(0);
        while (listIterator.hasNext()) {
            int value = (int) listIterator.next();
            if (value == 1) {
                listIterator.remove();
            }
        }
        Object[] result = {0, 2};
        assertEquals("DoublyLinkedListTest.testIteratorRemove", result, intDll.toArray());
    }

    public static void testIteratorSet() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        ListIterator listIterator = intDll.listIterator();
        if (listIterator.hasNext()) {
            listIterator.next();
            listIterator.set(3);
        }
        Object[] result = {3, 1, 2};
        assertEquals("DoublyLinkedListTest.testIteratorSet", result, intDll.toArray());
    }

    public static void testConcurrentModificationException() {
        DoublyLinkedList<Integer> intDll = new DoublyLinkedList<>();
        for (int i = 0; i < 3; i++) {
            intDll.add(i);
        }
        ListIterator listIterator = intDll.listIterator(0);

        String msg = "Test failed";
        try {
            while (listIterator.hasNext()) {
                Object value = listIterator.next();
                if (value.equals(2)) {
                    intDll.remove(value);
                }
            }
            fail(msg);
        } catch (ConcurrentModificationException ex) {
            assertEquals("DoublyLinkedListTest.testConcurrentModificationException", "ConcurrentModificationException", ex.getClass().getSimpleName());
        }
    }
}
