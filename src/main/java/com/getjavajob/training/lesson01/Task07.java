package com.getjavajob.training.algo08.tenetkok.lesson01;

import static com.getjavajob.training.algo08.util.Utility.readInt;

public class Task07 {

    public static void main(String[] args) {
        int a = readInt("Enter a: ");
        int b = readInt("Enter b: ");
        System.out.println(swapValues1(a, b));
        System.out.println(swapValues2(a, b));
        System.out.println(swapValues3(a, b));
        System.out.println(swapValues4(a, b));
    }

    public static String swapValues1(int a, int b) {
        int x = a + b;
        int y = x - b;
        x -= y;
        return x + " " + y;
    }

    public static String swapValues2(int a, int b) {
        int x = a * b;
        int y = x / b;
        x /= y;
        return x + " " + y;
    }

    public static String swapValues3(int a, int b) {
        int x = a ^ b;
        int y = x ^ b;
        x ^= y;
        return x + " " + y;
    }

    public static int getSum(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return getSum(a ^ b, (a & b) << 1);
        }
    }

    public static int getSub(int a, int b) {
        return (getSum(a, getSum(~b, 1)));
    }

    public static String swapValues4(int a, int b) {
        int x = getSum(a, b);
        int y = getSub(x, b);
        x = getSub(x, y);
        return x + " " + y;
    }
}