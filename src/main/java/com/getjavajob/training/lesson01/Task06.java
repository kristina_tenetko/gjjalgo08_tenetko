package com.getjavajob.training.algo08.tenetkok.lesson01;

import static com.getjavajob.training.algo08.util.Utility.readInt;

public class Task06 {

    public static void main(String[] args) {
        int n = readInt("Enter n: ");
        int m = readInt("Enter m: ");
        int a = readInt("Enter a: ");
        System.out.println("2^" + n + " = " + getTwoInPowerOfN(n));
        System.out.println("2^" + n + "+ 2^" + m + " = " + getSumOfPowersOfTwo(n, m));
        System.out.println(a + " with reset " + n + " lower bits: " + resetNLowerBits(a, n));
        System.out.println(a + " with " + n + "-th bit set with 1: " + setNBitWithOne(a, n));
        System.out.println(a + " with inverted " + n + "-th bit: " + invertNBit(a, n));
        System.out.println(a + " with " + n + "-th bit set with 0: " + setNBitWithZero(a, n));
        System.out.println(a + "'s " + n + " lower bits: " + returnNLowerBits(a, n));
        System.out.println(a + "'s " + n + "-th bit: " + returnNBit(a, n));
        System.out.println(a + "'s " + "bin representation: " + getBinRepresentation((byte) a));
    }

    /**
     * Calc 2^n
     *
     * @param n - power (n < 31)
     * @return 2^n
     */
    public static int getTwoInPowerOfN(int n) {
        return 1 << n;
    }

    /**
     * Calc 2^n + 2^m
     *
     * @param n - power (n < 31)
     * @param m - power (m < 31)
     * @return 2^n+2^m
     */
    public static int getSumOfPowersOfTwo(int n, int m) {
        if (m == 0) {
            return n;
        } else {
            int x = getTwoInPowerOfN(n);
            int y = getTwoInPowerOfN(m);
            return getSumOfPowersOfTwo(x ^ y, (x & y) << 1);
        }
    }

    /**
     * Reset n lower bits of a
     *
     * @param a - input number
     * @param n - the number of lower bits
     * @return a with reset n lower bits
     */
    public static int resetNLowerBits(int a, int n) {
        return a & (~1 << n - 1);
    }

    /**
     * Set a's n-th bit with 1
     *
     * @param a - input number
     * @param n - the number of bit
     * @return a with n-th bit set with 1
     */
    public static int setNBitWithOne(int a, int n) {
        return a | 1 << n;
    }

    /**
     * Invert n-th bit of a
     *
     * @param a - input number
     * @param n - the number of bit
     * @return a with inverted n-th bit
     */
    public static int invertNBit(int a, int n) {
        return a ^ 1 << n;
    }

    /**
     * Set a's n-th bit with 0
     *
     * @param a - input number
     * @param n - the number of bit
     * @return a with n-th bit set with 0
     */
    public static int setNBitWithZero(int a, int n) {
        return a & ~(1 << n);
    }

    /**
     * Return n lower bits
     *
     * @param a - input number
     * @param n - the number of bits
     * @return a's n lower bits
     */
    public static int returnNLowerBits(int a, int n) {
        return a & ~(~1 << n - 1);
    }

    /**
     * return n-th bit
     *
     * @param a - input number
     * @param n - the number of bit
     * @return a's n-th bit
     */
    public static int returnNBit(int a, int n) {
        return ((a & (1 << n)) == 1 << n) ? 1 : 0;
    }

    /**
     * output bin representation of byte
     *
     * @param a - input byte number
     * @return bin representation of a
     */
    public static String getBinRepresentation(byte a) {
        StringBuilder builder = new StringBuilder();
        int mask = 0b100000;
        if ((a & 0b10000000) == 0) {
            builder.append('0');
        } else {
            builder.append('1');
        }
        while (mask != 0) {
            if ((a & mask) == 0) {
                builder.append('0');
            } else {
                builder.append('1');
            }
            mask = mask >> 1;
        }
        return builder.toString();
    }
}
