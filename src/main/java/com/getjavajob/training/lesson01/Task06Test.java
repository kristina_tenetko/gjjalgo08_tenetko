package com.getjavajob.training.algo08.tenetkok.lesson01;

import static com.getjavajob.training.algo08.tenetkok.lesson01.Task06.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class Task06Test {

    public static void main(String[] args) {
        testGetTwoInPowerOfN();
        testGetSumOfPowersOfTwo();
        testResetNLowerBits();
        testSetNBitWithOne();
        testInvertNBit();
        testSetNBitWithZero();
        testReturnNLowerBits();
        testReturnNBit();
        testGetBinRepresentation();
    }

    private static void testGetTwoInPowerOfN() {
        assertEquals("TaskCh06Test.testGetTwoInPowerOfN", 0b100000, getTwoInPowerOfN(5));
    }

    private static void testGetSumOfPowersOfTwo() {
        assertEquals("TaskCh06Test.testGetSumOfPowersOfTwo", 0b1100, getSumOfPowersOfTwo(2, 3));
    }

    private static void testResetNLowerBits() {
        assertEquals("TaskCh06Test.testResetNLowerBits", 0b11000, resetNLowerBits(0b11101, 3));
    }

    private static void testSetNBitWithOne() {
        assertEquals("TaskCh06Test.testSetNBitWithOne", 0b11111, setNBitWithOne(0b11101, 1));
    }

    private static void testInvertNBit() {
        assertEquals("TaskCh06Test.testInvertNBit", 0b10101, invertNBit(0b11101, 3));
    }

    private static void testSetNBitWithZero() {
        assertEquals("TaskCh06Test.testSetNBitWithZero", 0b11100, setNBitWithZero(0b11101, 0));
    }

    private static void testReturnNLowerBits() {
        assertEquals("TaskCh06Test.testReturnNLowerBits", 0b101, returnNLowerBits(0b11101, 3));
    }

    private static void testReturnNBit() {
        assertEquals("TaskCh06Test.testReturnNBit", 1, returnNBit(0b11101, 3));
    }

    private static void testGetBinRepresentation() {
        assertEquals("TaskCh06Test.testGetBinRepresentation", "0011101", getBinRepresentation((byte) 29));
    }
}
