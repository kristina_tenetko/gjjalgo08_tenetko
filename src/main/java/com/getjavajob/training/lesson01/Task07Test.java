package com.getjavajob.training.algo08.tenetkok.lesson01;

import static com.getjavajob.training.algo08.tenetkok.lesson01.Task07.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class Task07Test {
    public static void main(String[] args) {
        testSwapValues1();
        testSwapValues2();
        testSwapValues3();
        testSwapValues4();
    }

    private static void testSwapValues1() {
        assertEquals("TaskCh06Test.testSwapValues1", "4 5", swapValues1(5, 4));
    }

    private static void testSwapValues2() {
        assertEquals("TaskCh06Test.testSwapValues2", "4 5", swapValues2(5, 4));
    }

    private static void testSwapValues3() {
        assertEquals("TaskCh06Test.testSwapValues3", "4 5", swapValues3(5, 4));
    }

    private static void testSwapValues4() {
        assertEquals("TaskCh06Test.testSwapValues4", "4 5", swapValues4(5, 4));
    }
}