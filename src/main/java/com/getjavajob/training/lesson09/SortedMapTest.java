package com.getjavajob.training.algo08.tenetkok.lesson09;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class SortedMapTest {

    private static SortedMap<Integer, String> map = new TreeMap<>();

    public static void main(String[] args) {
        mapInit();
        comparatorTest();
        subMapTest();
        headMapTest();
        tailMapTest();
        firstKeyTest();
        lastKeyTest();
        keySetTest();
        valuesTest();
        entrySetTest();
    }

    private static void mapInit() {
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
        map.put(6, "six");
        map.put(7, "seven");
        map.put(8, "eight");
        map.put(9, "nine");
        map.put(10, "ten");
    }

    public static void comparatorTest() {
        assertEquals("SortedMapTest.comparatorTest", null, map.comparator());
    }

    public static void subMapTest() {
        SortedMap<Integer, String> sortedMap = map.subMap(2, 4);
        List<String> list = new ArrayList<>(sortedMap.values());
        assertEquals("SortedMapTest.subMapTest", "[two, three]", list.toString());
    }

    public static void headMapTest() {
        SortedMap<Integer, String> sortedMap = map.headMap(3);
        List<String> list = new ArrayList<>(sortedMap.values());
        assertEquals("SortedMapTest.headMapTest", "[one, two]", list.toString());
    }

    public static void tailMapTest() {
        SortedMap<Integer, String> sortedMap = map.tailMap(5);
        List<String> list = new ArrayList<>(sortedMap.values());
        assertEquals("SortedMapTest.tailMapTest", "[five, six, seven, eight, nine, ten]", list.toString());
    }

    public static void firstKeyTest() {
        Object first = map.firstKey();
        assertEquals("SortedMapTest.firstKeyTest", 1, first);
    }

    public static void lastKeyTest() {
        Object last = map.lastKey();
        assertEquals("SortedMapTest.lastKeyTest", 10, last);
    }

    public static void keySetTest() {
        Set<Integer> set = map.keySet();
        assertEquals("SortedMapTest.keySetTest", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]", set.toString());
    }

    public static void valuesTest() {
        Collection<String> collection = map.values();
        assertEquals("SortedMapTest.valuesTest", "[one, two, three, four, five, six, seven, eight, nine, ten]", collection.toString());
    }

    public static void entrySetTest() {
        Set<Map.Entry<Integer, String>> setMap = map.entrySet();
        Iterator<Map.Entry<Integer, String>> it = setMap.iterator();
        StringBuilder builder = new StringBuilder();
        while (it.hasNext()) {
            builder.append(it.next()).append(' ');
        }
        assertEquals("SortedMapTest.entrySetTest", builder.toString(), "1=one 2=two 3=three 4=four 5=five 6=six 7=seven 8=eight 9=nine 10=ten ");
    }
}
