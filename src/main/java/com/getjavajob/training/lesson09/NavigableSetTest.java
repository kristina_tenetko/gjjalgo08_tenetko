package com.getjavajob.training.algo08.tenetkok.lesson09;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class NavigableSetTest {

    private static NavigableSet<Integer> set = new TreeSet<>();

    public static void main(String[] args) {
        setInit();
        lowerTest();
        floorTest();
        ceilingTest();
        higherTest();
        pollFirstTest();
        pollLastTest();
        iteratorSet();
    }

    private static void setInit() {
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(6);
        set.add(7);
        set.add(8);
        set.add(9);
        set.add(10);
    }

    public static void lowerTest() {
        assertEquals("NavigableSetTest.lowerTest1", 2, set.lower(3));
        assertEquals("NavigableSetTest.lowerTest2", null, set.lower(1));
    }

    public static void floorTest() {
        assertEquals("NavigableSetTest.floorTest1", 10, set.floor(15));
        assertEquals("NavigableSetTest.floorTest2", 5, set.floor(5));
        assertEquals("NavigableSetTest.floorTest3", null, set.floor(-10));
    }

    public static void ceilingTest() {
        assertEquals("NavigableSetTest.ceilingTest1", 1, set.ceiling(-10));
        assertEquals("NavigableSetTest.ceilingTest2", null, set.ceiling(11));
    }

    public static void higherTest() {
        assertEquals("NavigableSetTest.higherTest1", 1, set.higher(-9));
        assertEquals("NavigableSetTest.higherTest2", 10, set.higher(9));
    }

    public static void pollFirstTest() {
        TreeSet<Integer> set = new TreeSet<>();
        set.add(1);
        set.add(2);
        assertEquals("NavigableSetTest.pollFirstTest1", 1, set.pollFirst());
        assertEquals("NavigableSetTest.pollFirstTest2", 2, set.pollFirst());
        assertEquals("NavigableSetTest.pollFirstTest3", null, set.pollFirst());
    }

    public static void pollLastTest() {
        TreeSet<Integer> set = new TreeSet<>();
        set.add(1);
        set.add(2);
        assertEquals("NavigableSetTest.pollLastTest1", 2, set.pollLast());
        assertEquals("NavigableSetTest.pollLastTest2", 1, set.pollLast());
        assertEquals("NavigableSetTest.pollLastTest3", null, set.pollLast());
    }

    public static void iteratorSet() {
        Iterator<Integer> it = set.iterator();
        StringBuilder builder = new StringBuilder();
        while (it.hasNext()) {
            builder.append(it.next());
        }
        assertEquals("NavigableSetTest.iteratorSet", "12345678910", builder.toString());
    }
}
