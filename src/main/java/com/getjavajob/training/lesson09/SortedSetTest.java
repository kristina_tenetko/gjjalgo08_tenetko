package com.getjavajob.training.algo08.tenetkok.lesson09;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class SortedSetTest {

    private static SortedSet<Integer> set = new TreeSet<>();

    public static void main(String[] args) {
        setInit();
        subSetTest();
        headSetTest();
        tailSetTest();
        firstTest();
        lastTest();
    }

    private static void setInit() {
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(6);
        set.add(7);
        set.add(8);
        set.add(9);
        set.add(10);
    }

    public static void subSetTest() {
        Set<Integer> sub = set.subSet(3, 8);
        List<Integer> list = new LinkedList<>(sub);
        assertEquals("SortedSetTest.subSetTest", "[3, 4, 5, 6, 7]", list.toString());
    }

    public static void headSetTest() {
        SortedSet<Integer> head = set.headSet(4);
        List<Integer> list = new LinkedList<>(head);
        assertEquals("SortedSetTest.headSetTest", "[1, 2, 3]", list.toString());
    }

    public static void tailSetTest() {
        SortedSet<Integer> tail = set.tailSet(4);
        List<Integer> list = new LinkedList<>(tail);
        assertEquals("SortedSetTest.tailSetTest", "[4, 5, 6, 7, 8, 9, 10]", list.toString());
    }

    public static void firstTest() {
        Object first = set.first();
        assertEquals("SortedSetTest.firstTest", 1, first);
    }

    public static void lastTest() {
        Object tail = set.last();
        assertEquals("SortedSetTest.lastTest", 10, tail);
    }
}

