package com.getjavajob.training.algo08.tenetkok.lesson09;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class LookUpCityStorageTest {

    private static LookUpCityStorage storage = new LookUpCityStorage();

    public static void main(String[] args) {
        initStorage();
        testSearch();
        testSearchLowecase();
        testSearchNotFound();
    }

    private static void initStorage() {
        storage.add("Moscow");
        storage.add("Mogilev");
        storage.add("Khabarovsk");
        storage.add("Orenburg");
        storage.add("Orel");
        storage.add("Omsk");
        storage.add("Ufa");
    }

    private static void testSearch() {
        Collection<String> result = storage.search("O");
        SortedSet<String> set = new TreeSet<>(result);
        List<String> list = new ArrayList<>(set);
        assertEquals("LookUpCityStorageTest.testSearch", "[Omsk, Orel, Orenburg]", list.toString());
    }

    private static void testSearchLowecase() {
        Collection<String> result = storage.search("mo");
        SortedSet<String> set = new TreeSet<>(result);
        List<String> list = new ArrayList<>(set);
        assertEquals("LookUpCityStorageTest.testSearchLowecase", "[Mogilev, Moscow]", list.toString());
    }

    private static void testSearchNotFound() {
        Collection<String> result = storage.search("Q");
        SortedSet<String> set = new TreeSet<>(result);
        List<String> list = new ArrayList<>(set);
        assertEquals("LookUpCityStorageTest.testSearchNotFound", "[]", list.toString());
    }
}
