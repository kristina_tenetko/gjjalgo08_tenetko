package com.getjavajob.training.algo08.tenetkok.lesson09;

import com.getjavajob.training.algo08.tenetkok.lesson07.Node;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class RedBlackTreeTest {

    public static void main(String[] args) {
        testAddLeftFourTimes();
        testAddRightFourTimes();
        testAddLeftRight();
        testAddRightLeft();
        testRemoveSimple();
        testRemove();
    }

    private static void testAddLeftFourTimes() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        Node<Integer> n1 = tree.addRoot(10);
        Node<Integer> n11 = tree.add(n1, 9);
        Node<Integer> n111 = tree.add(n11, 8);
        Node<Integer> n1111 = tree.add(n111, 7);
        Node<Integer> n11111 = tree.add(n1111, 6);

        assertEquals("RedBlackTreeTest.testAddLeftFourTimes", "(9(7(6, 8), 10))", tree.stringRepresentation());

    }

    private static void testAddRightFourTimes() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        Node<Integer> n1 = tree.addRoot(10);
        Node<Integer> n11 = tree.add(n1, 11);
        Node<Integer> n111 = tree.add(n11, 12);
        Node<Integer> n1111 = tree.add(n111, 13);
        Node<Integer> n11111 = tree.add(n1111, 14);

        assertEquals("RedBlackTreeTest.testAddRightFourTimes", "(11(10, 13(12, 14)))", tree.stringRepresentation());

    }

    private static void testAddLeftRight() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        Node<Integer> n1 = tree.addRoot(10);
        Node<Integer> n11 = tree.add(n1, 5);
        Node<Integer> n111 = tree.add(n11, 8);

        assertEquals("RedBlackTreeTest.testAddLeftRight", "(8(5, 10))", tree.stringRepresentation());

    }

    private static void testAddRightLeft() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        Node<Integer> n1 = tree.addRoot(10);
        Node<Integer> n11 = tree.add(n1, 16);
        Node<Integer> n111 = tree.add(n11, 13);

        assertEquals("RedBlackTreeTest.testAddRightLeft", "(13(10, 16))", tree.stringRepresentation());

    }

    private static void testRemoveSimple() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        Node<Integer> n1 = tree.addRoot(10);
        Node<Integer> n11 = tree.add(n1, 11);
        Node<Integer> n111 = tree.add(n11, 12);
        Node<Integer> n1111 = tree.add(n111, 13);
        Node<Integer> n11111 = tree.add(n1111, 14);
        tree.remove(n111);

        assertEquals("RedBlackTreeTest.testRemoveSimple()", "(11(10, 13(null, 14)))", tree.stringRepresentation());
    }

    private static void testRemove() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        Node<Integer> n1 = tree.addRoot(11);
        Node<Integer> n11 = tree.add(n1, 7);
        Node<Integer> n111 = tree.add(n1, 12);
        Node<Integer> n1111 = tree.add(n111, 13);
        Node<Integer> n11111 = tree.add(n11, 5);
        Node<Integer> n111111 = tree.add(n11, 10);
        Node<Integer> n1111111 = tree.add(n111111, 9);
        tree.remove(n11);

        assertEquals("RedBlackTreeTest.testRemove()", "(11(9(5, 10), 12(null, 13)))", tree.stringRepresentation());
    }
}
