package com.getjavajob.training.algo08.tenetkok.lesson09;

import java.util.*;

public class LookUpCityStorage {

    private NavigableMap<String, String> storage = new TreeMap<>();

    public void add(String city) {
        storage.put(city.toLowerCase(), city);
    }

    private SortedMap<String, String> searchBySub(String subString) {
        String subFrom = subString.toLowerCase();
        String subTo = subFrom.substring(0, subFrom.length() - 1) + (char) (subFrom.charAt(subFrom.length() - 1) + 1);
        return storage.subMap(subFrom, subTo);
    }

    public Collection<String> search(String question) {
        return new ArrayList<>(searchBySub(question).values());
    }

}
