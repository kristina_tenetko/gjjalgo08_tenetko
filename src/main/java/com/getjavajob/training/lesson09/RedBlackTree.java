package com.getjavajob.training.algo08.tenetkok.lesson09;

import com.getjavajob.training.algo08.tenetkok.lesson07.Node;
import com.getjavajob.training.algo08.tenetkok.lesson08.binary.search.balanced.BalanceableTree;

public class RedBlackTree<E> extends BalanceableTree<E> {

    @Override
    protected NodeImpl<E> createNode(E e) {
        return new NodeImplColor<>(e);
    }

    @Override
    protected NodeImplColor<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n instanceof NodeImplColor) {
            return (NodeImplColor<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    private boolean isBlack(Node<E> n) {
        return n != null && validate(n).isBlack();
    }

    private boolean isRed(Node<E> n) {
        return n != null && validate(n).isRed();
    }

    private void makeBlack(Node<E> n) {
        validate(n).setBlack();
    }

    private void makeRed(Node<E> n) {
        validate(n).setRed();
    }

    @Override
    protected void afterElementAdded(Node<E> n) {
        NodeImplColor<E> node = validate(n);
        if (node == root()) {
            node.setBlack();
        } else {
            NodeImplColor<E> parent = validate(node.getParent());
            NodeImplColor<E> uncle = sibling(parent) != null ? validate(sibling(parent)) : null;
            NodeImplColor<E> grandparent = parent.getParent() != null ? validate(parent.getParent()) : null;

            if (n != null && n != root && parent.isRed()) {
                if (uncle != null && uncle.isRed()) {
                    parent.setBlack();
                    uncle.setBlack();
                    if (grandparent != null) {
                        grandparent.setRed();
                    }
                    afterElementAdded(grandparent);
                } else if (parent.isLeftChild()) {
                    if (node.isRightChild()) {
                        rotate(node);
                        node = validate(node.getLeft());
                    }
                    validate(node.getParent()).setBlack();
                    if (grandparent != null) {
                        grandparent.setRed();
                    }
                    rotate(node.getParent());
                } else if (parent.isRightChild()) {
                    if (node.isLeftChild()) {
                        rotate(node);
                        node = validate(node.getRight());
                    }
                    validate(node.getParent()).setBlack();
                    if (grandparent != null) {
                        grandparent.setRed();
                    }
                    rotate(node.getParent());
                }
            }
            validate(root()).setBlack();
        }
    }

    @Override
    protected void afterElementRemoved(Node<E> n) {
        NodeImplColor<E> node = validate(n);
        while (node != root && isBlack(node)) {
            if (node.isLeftChild()) {
                // Pulled up node is a left child
                NodeImplColor<E> sibling = validate(sibling(node));
                if (isRed(sibling)) {
                    sibling.isBlack();
                    validate(node.getParent()).setRed();
                    rotate(node);
                    sibling = validate(sibling(node));
                }
                if (isBlack(sibling.getLeft()) && isBlack(sibling.getRight())) {
                    sibling.setRed();
                    node = validate(node.getParent());
                } else {
                    if (isBlack(sibling.getRight())) {
                        makeBlack(sibling.getLeft());
                        makeRed(sibling);
                        rotate(sibling.getRight());
                        sibling = validate(validate(node.getParent()).getRight());
                    }
                    if (validate(node.getParent()).isBlack()) {
                        makeBlack(sibling);
                    } else {
                        makeRed(sibling);
                    }
                    makeBlack(parent(node));
                    makeBlack(sibling.getRight());
                    rotate(node);
                    node = validate(root);
                }
            } else {
                // pulled up node is a right child
                NodeImplColor<E> sibling = validate(sibling(node));
                if (isRed(sibling)) {
                    makeBlack(sibling);
                    makeRed(node.getParent());
                    rotate(node);
                    sibling = validate(sibling(node));
                }
                if (isBlack(sibling.getLeft()) && isBlack(sibling.getRight())) {
                    makeRed(sibling);
                    node = validate(node.getParent());
                } else {
                    if (isBlack(sibling.getLeft())) {
                        makeBlack(sibling.getRight());
                        makeRed(sibling);
                        rotate(sibling.getLeft());
                        sibling = validate(validate(node.getParent()).getLeft());
                    }
                    if (validate(node.getParent()).isBlack()) {
                        makeBlack(sibling);
                    } else {
                        makeRed(sibling);
                    }

                    makeBlack(node.getParent());
                    makeBlack(sibling.getLeft());
                    rotate(node);
                    n = validate(root);
                }
            }
        }
        makeBlack(node);

    }

    protected static class NodeImplColor<E> extends NodeImpl<E> {
        boolean red;

        public NodeImplColor(E data) {
            super(data);
            this.red = true;
        }

        public boolean isRed() {
            return red;
        }

        public boolean isBlack() {
            return !isRed();
        }

        public void setRed() {
            this.red = true;
        }

        public void setBlack() {
            this.red = false;
        }
    }
}
