package com.getjavajob.training.algo08.tenetkok.lesson09;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class NavigableMapTest {

    private static NavigableMap<Integer, String> map = new TreeMap<>();

    public static void main(String[] args) {
        mapInit();
        lowerEntryTest();
        lowerKeyTest();
        floorEntryTest();
        floorKeyTest();
        ceilingEntryTest();
        ceilingKeyTest();
        higherEntryTest();
        higherKeyTest();
        firstEntryTest();
        lastEntryTest();
        pollFirstEntryTest();
        pollLastEntryTest();
        descendingMapTest();
        navigableKeySetTest();
        descendingKeySetTest();
        subMapTest();
        headMapTest();
        tailMapTest();
        subSortedMapTest();
        headSortedMapTest();
        tailSortedMapTest();
    }

    private static void mapInit() {
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
        map.put(6, "six");
        map.put(7, "seven");
    }

    public static void lowerEntryTest() {
        Map.Entry<Integer, String> myMap = map.lowerEntry(5);
        assertEquals("NavigableMapTest.lowerEntryTest1", "4=four", myMap.toString());
        myMap = map.lowerEntry(1);
        assertEquals("NavigableMapTest.lowerEntryTest2", null, myMap);
    }

    public static void lowerKeyTest() {
        Integer key = map.lowerKey(5);
        assertEquals("NavigableMapTest.lowerKeyTest1", 4, key);
        key = map.lowerKey(1);
        assertEquals("NavigableMapTest.lowerKeyTest2", null, key);
    }

    public static void floorEntryTest() {
        Map.Entry<Integer, String> myMap = map.floorEntry(5);
        assertEquals("NavigableMapTest.floorEntryTest1", "5=five", myMap.toString());
        myMap = map.floorEntry(-1);
        assertEquals("NavigableMapTest.floorEntryTest2", null, myMap);
    }

    public static void floorKeyTest() {
        Integer key = map.floorKey(5);
        assertEquals("NavigableMapTest.floorKeyTest1", 5, key);
        key = map.floorKey(0);
        assertEquals("NavigableMapTest.floorKeyTest2", null, key);
    }

    public static void ceilingEntryTest() {
        Map.Entry<Integer, String> myMap = map.ceilingEntry(-1);
        assertEquals("NavigableMapTest.ceilingEntryTest1", "1=one", myMap.toString());
        myMap = map.ceilingEntry(5);
        assertEquals("NavigableMapTest.ceilingEntryTest2", "5=five", myMap.toString());
        myMap = map.ceilingEntry(8);
        assertEquals("NavigableMapTest.ceilingEntryTest3", null, myMap);
    }

    public static void ceilingKeyTest() {
        Integer key = map.ceilingKey(-1);
        assertEquals("NavigableMapTest.ceilingKeyTest1", 1, key);
        key = map.ceilingKey(5);
        assertEquals("NavigableMapTest.ceilingKeyTest2", 5, key);
        key = map.ceilingKey(8);
        assertEquals("NavigableMapTest.ceilingKeyTest3", null, key);
    }

    public static void higherEntryTest() {
        Map.Entry<Integer, String> myMap = map.higherEntry(-1);
        assertEquals("NavigableMapTest.higherEntryTest1", myMap.toString(), "1=one");
        myMap = map.higherEntry(5);
        assertEquals("NavigableMapTest.higherEntryTest2", myMap.toString(), "6=six");
        myMap = map.higherEntry(8);
        assertEquals("NavigableMapTest.higherEntryTest3", myMap, null);
    }

    public static void higherKeyTest() {
        Integer key = map.higherKey(-1);
        assertEquals("NavigableMapTest.higherKeyTest1", 1, key);
        key = map.higherKey(5);
        //noinspection UnnecessaryBoxing
        assertEquals("NavigableMapTest.higherKeyTest2", 6, key);
        key = map.higherKey(8);
        assertEquals("NavigableMapTest.higherKeyTest3", null, key);
    }

    public static void firstEntryTest() {
        Map.Entry<Integer, String> myMap = map.firstEntry();
        assertEquals("NavigableMapTest.firstEntryTest1", "1=one", myMap.toString());
        assertEquals("NavigableMapTest.firstEntryTest2", null, new TreeMap<Integer, String>().firstEntry());
    }

    public static void lastEntryTest() {
        Map.Entry<Integer, String> myMap = map.lastEntry();
        assertEquals("NavigableMapTest.lastEntryTest1", "7=seven", myMap.toString());
        assertEquals("NavigableMapTest.lastEntryTest2", null, new TreeMap<Integer, String>().lastEntry());
    }

    public static void pollFirstEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");

        Map.Entry<Integer, String> myMap = map.pollFirstEntry();
        assertEquals("NavigableMapTest.pollFirstEntryTest1", "1=one", myMap.toString());
        myMap = map.pollFirstEntry();
        assertEquals("NavigableMapTest.pollFirstEntryTest2", "2=two", myMap.toString());
        myMap = map.pollFirstEntry();
        assertEquals("NavigableMapTest.pollFirstEntryTest3", null, myMap);
    }

    public static void pollLastEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");

        Map.Entry<Integer, String> myMap = map.pollLastEntry();
        assertEquals("NavigableMapTest.pollLastEntryTest1", "2=two", myMap.toString());
        myMap = map.pollLastEntry();
        assertEquals("NavigableMapTest.pollLastEntryTest2", "1=one", myMap.toString());
        myMap = map.pollLastEntry();
        assertEquals("NavigableMapTest.pollLastEntryTest3", null, myMap);
    }

    public static void descendingMapTest() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");

        NavigableMap<Integer, String> myMap = map.descendingMap();
        assertEquals("NavigableMapTest.descendingMapTest", "{2=two, 1=one}", myMap.toString());
    }

    public static void navigableKeySetTest() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");

        NavigableSet<Integer> myMap = map.navigableKeySet();
        assertEquals("NavigableMapTest.navigableKeySetTest", "[1, 2]", myMap.toString());
    }

    public static void descendingKeySetTest() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");

        NavigableSet<Integer> myMap = map.descendingKeySet();
        assertEquals("NavigableMapTest.descendingKeySetTest", "[2, 1]", myMap.toString());
    }

    public static void subMapTest() {
        NavigableMap<Integer, String> myMap = (NavigableMap<Integer, String>) map.subMap(2, 4);
        assertEquals("NavigableMapTest.subMapTest", "{2=two, 3=three}", myMap.toString());
    }

    public static void headMapTest() {
        NavigableMap<Integer, String> myMap = (NavigableMap<Integer, String>) map.headMap(4);
        assertEquals("NavigableMapTest.headMapTest", "{1=one, 2=two, 3=three}", myMap.toString());
    }

    public static void tailMapTest() {
        NavigableMap<Integer, String> myMap = (NavigableMap<Integer, String>) map.tailMap(6);
        assertEquals("NavigableMapTest.tailMapTest", "{6=six, 7=seven}", myMap.toString());
    }

    public static void subSortedMapTest() {
        SortedMap<Integer, String> myMap = map.subMap(2, 4);
        assertEquals("NavigableMapTest.subSortedMapTest", "{2=two, 3=three}", myMap.toString());
    }

    public static void headSortedMapTest() {
        SortedMap<Integer, String> myMap = map.headMap(4);
        assertEquals("NavigableMapTest.headSortedMapTest", "{1=one, 2=two, 3=three}", myMap.toString());
    }

    public static void tailSortedMapTest() {
        SortedMap<Integer, String> myMap = map.tailMap(6);
        assertEquals("NavigableMapTest.tailSortedMapTest", "{6=six, 7=seven}", myMap.toString());
    }
}
