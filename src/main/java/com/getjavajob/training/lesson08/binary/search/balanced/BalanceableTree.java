package com.getjavajob.training.algo08.tenetkok.lesson08.binary.search.balanced;

import com.getjavajob.training.algo08.tenetkok.lesson07.Node;
import com.getjavajob.training.algo08.tenetkok.lesson08.binary.search.BinarySearchTree;

public class BalanceableTree<E> extends BinarySearchTree<E> {

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(com.getjavajob.training.algo08.tenetkok.lesson07.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent        new parent
     * @param child         new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {

        if (parent == root()) {
            child.setParent(null);
            root = child;
        } else {
            if (parent.isLeftChild()) {
                parent.getParent().setLeft(child);
            } else {
                parent.getParent().setRight(child);
            }
            child.setParent(parent.getParent());
        }
        parent.setParent(child);

        if (makeLeftChild) {
            if (child.getRight() != null) {
                child.getRight().setParent(parent);
            }
            parent.setLeft(child.getRight());
            child.setRight(parent);
        } else {
            if (child.getLeft() != null) {
                child.getLeft().setParent(parent);
            }
            parent.setRight(child.getLeft());
            child.setLeft(parent);
        }
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        NodeImpl<E> node = validate(n);
        boolean makeLeftChild = compare(n.getElement(), node.getParent().getElement()) < 0;
        relink(node.getParent(), node, makeLeftChild);
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(com.getjavajob.training.algo08.tenetkok.lesson07.Node)} to reduce the height of subtree rooted at <i>n1</i>
     * <p>
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        NodeImpl<E> node = validate(n);
        NodeImpl<E> parent = validate(node.getParent());

        if (node.getParent() != null && parent.getParent() != null) {
            if (node.isLeftChild() && parent.isLeftChild()) {
                relink(parent.getParent(), parent, true);
                return node.getParent();
            } else if (node.isRightChild() && parent.isRightChild()) {
                relink(parent.getParent(), parent, false);
                return node.getParent();
            } else if (node.isRightChild() && parent.isLeftChild()) {
                relink(parent, node, false);
                relink(node.getParent(), node, true);
                return node;
            } else {
                relink(parent, node, true);
                relink(node.getParent(), node, false);
                return node;
            }
        }
        return null;
    }
}
