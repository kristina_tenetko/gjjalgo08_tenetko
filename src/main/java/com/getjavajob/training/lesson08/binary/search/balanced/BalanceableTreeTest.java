package com.getjavajob.training.algo08.tenetkok.lesson08.binary.search.balanced;

import com.getjavajob.training.algo08.tenetkok.lesson07.Node;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class BalanceableTreeTest {

    public static void main(String[] args) {
        testRotate();
        testReduceSubtreeHeightLeftLeftCase();
        testReduceSubtreeHeightRightRightCase();
        testReduceSubtreeHeightLeftRightCase();
        testReduceSubtreeHeightRightLeftCase();
    }

    private static void testReduceSubtreeHeightLeftLeftCase() {
        BalanceableTree<Integer> tree = new BalanceableTree<>();
        Node<Integer> n1 = tree.addRoot(50);
        Node<Integer> n11 = tree.add(n1, 40);
        Node<Integer> n111 = tree.add(n11, 30);
        Node<Integer> newRoot = tree.reduceSubtreeHeight(n111);
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightLeftLeftCase()", "(40(30, 50))", tree.stringRepresentation());
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightLeftLeftCase()", 40, newRoot.getElement());
    }

    private static void testReduceSubtreeHeightRightRightCase() {
        BalanceableTree<Integer> tree = new BalanceableTree<>();
        Node<Integer> n1 = tree.addRoot(50);
        Node<Integer> n11 = tree.add(n1, 60);
        Node<Integer> n111 = tree.add(n11, 70);
        Node<Integer> newRoot = tree.reduceSubtreeHeight(n111);
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightRightRightCase()", "(60(50, 70))", tree.stringRepresentation());
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightRightRightCase()", 60, newRoot.getElement());
    }

    private static void testReduceSubtreeHeightLeftRightCase() {
        BalanceableTree<Integer> tree = new BalanceableTree<>();
        Node<Integer> n1 = tree.addRoot(50);
        Node<Integer> n11 = tree.add(n1, 40);
        Node<Integer> n111 = tree.add(n11, 45);
        Node<Integer> newRoot = tree.reduceSubtreeHeight(n111);
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightLeftRightCase()", "(45(40, 50))", tree.stringRepresentation());
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightLeftRightCase()", 45, newRoot.getElement());
    }

    private static void testReduceSubtreeHeightRightLeftCase() {
        BalanceableTree<Integer> tree = new BalanceableTree<>();
        Node<Integer> n1 = tree.addRoot(50);
        Node<Integer> n11 = tree.add(n1, 60);
        Node<Integer> n111 = tree.add(n11, 55);
        Node<Integer> newRoot = tree.reduceSubtreeHeight(n111);
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightRightLeftCase()", "(55(50, 60))", tree.stringRepresentation());
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeightRightLeftCase()", 55, newRoot.getElement());
    }


    private static void testRotate() {
        BalanceableTree<Integer> tree = new BalanceableTree<>();
        Node<Integer> n1 = tree.addRoot(67);
        Node<Integer> n11 = tree.addLeft(n1, 36);
        Node<Integer> n112 = tree.addRight(n11, 37);
        Node<Integer> n111 = tree.addLeft(n11, 12);
        Node<Integer> n1112 = tree.addRight(n111, 14);
        Node<Integer> n12 = tree.addRight(n1, 72);
        tree.rotate(n11);
        assertEquals("BalanceableTreeTest.testRotateLeft", "(36(12(null, 14), 67(37, 72)))", tree.stringRepresentation());
        tree.rotate(tree.right(tree.root()));
        assertEquals("BalanceableTreeTest.testRotateRight", "(67(36(12(null, 14), 37), 72))", tree.stringRepresentation());
    }
}
