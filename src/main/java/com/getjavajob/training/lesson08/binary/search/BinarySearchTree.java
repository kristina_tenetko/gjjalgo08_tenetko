package com.getjavajob.training.algo08.tenetkok.lesson08.binary.search;

import com.getjavajob.training.algo08.tenetkok.lesson07.Node;
import com.getjavajob.training.algo08.tenetkok.lesson07.binary.LinkedBinaryTree;

import java.util.Comparator;

/**
 * @author Vital Severyn
 * @since 31.07.15
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {

    private Comparator<E> comparator;

    public BinarySearchTree() {
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    private NodeImpl<E> minimumElement(NodeImpl<E> root) {
        if (root.getLeft() == null) {
            return root;
        }
        else {
            return minimumElement(root.getLeft());
        }
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        Node<E> addedNode = super.addRoot(e);
        afterElementAdded(addedNode);
        return addedNode;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (compare(n.getElement(), e) == 0) {
            return null;
        } else if (compare(n.getElement(), e) < 0) {
            return addRight(n, e);
        } else {
            return addLeft(n, e);
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        if (compare(n.getElement(), e) == 0) {
            return null;
        } else if (compare(n.getElement(), e) > 0) {
            Node<E> addedNode = super.addLeft(n, e);
            afterElementAdded(addedNode);
            return addedNode;
        } else {
            throw new IllegalArgumentException("Can't insert bigger element to left");
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        if (compare(n.getElement(), e) == 0) {
            return null;
        } else if (compare(n.getElement(), e) < 0) {
            Node<E> addedNode = super.addRight(n, e);
            afterElementAdded(addedNode);
            return addedNode;
        } else {
            throw new IllegalArgumentException("Can't insert smaller element to right");
        }
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.getLeft() == null || node.getRight() == null) {
            E deletedValue = super.remove(n);
            afterElementRemoved(node);
            return deletedValue;
        } else {
            NodeImpl<E> minNodeForRight = minimumElement(node.getRight());
            E deletedValue = node.getElement();
            node.setElement(minNodeForRight.getElement());
            remove(minNodeForRight);
            afterElementRemoved(node);
            return deletedValue;
        }
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */

    protected int compare(E val1, E val2) {
        if (val1 != null && val2 != null) {
            if (comparator != null) {
                return comparator.compare(val1, val2);
            }
            return ((Comparable<E>) val1).compareTo(val2);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n
     * @param val
     * @return
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        if (n == null) {
            return null;
        }
        NodeImpl<E> node = validate(n);
        int res = compare(node.getElement(), val);
        if (res == 0) {
            return node;
        } else if (res > 0) {
            return treeSearch(node.getLeft(), val);
        } else if (res < 0) {
            return treeSearch(node.getRight(), val);
        } else {
            return null;
        }
    }

    protected void afterElementRemoved(Node<E> n) {

    }

    protected void afterElementAdded(Node<E> n) {

    }

    public String stringRepresentation() {
        StringBuilder result = new StringBuilder();
        if (root() != null) {
            result.append('(').append(getElementString(root));
            NodeImpl<E> currentNode = validate(root());
            NodeImpl<E> holder;
            boolean fromRightChild = false;
            while (true) {
                while (fromRightChild) {
                    if (currentNode == root()) {
                        result.append(')');
                        return result.toString();
                    }
                    holder = currentNode;
                    currentNode = currentNode.getParent();
                    fromRightChild = currentNode.getRight() == holder;
                    if (!fromRightChild && currentNode.getRight() != null) {
                        if (currentNode.getLeft() != null) {
                            result.append(", ").append(getElementString(currentNode.getRight()));
                        } else {
                            result.append("(null, ").append(getElementString(currentNode.getRight()));
                        }
                        currentNode = currentNode.getRight();
                    } else {
                        fromRightChild = true;
                        result.append(")");
                    }
                }

                while (currentNode.getLeft() != null) {
                    result.append('(').append(getElementString(currentNode.getLeft()));
                    currentNode = currentNode.getLeft();
                }
                if (currentNode.getRight() != null) {
                    if (currentNode.getLeft() != null) {
                        result.append(", ").append(getElementString(currentNode.getRight()));
                    } else {
                        result.append("(null, ").append(getElementString(currentNode.getRight()));
                    }
                    currentNode = currentNode.getRight();
                } else {
                    fromRightChild = true;
                }
            }
        }

        return result.toString();
    }

    private String getElementString(Node<E> node) {
        if (node.getElement() != null) {
            return (node.getElement().toString().length() == 0) ? "--- " : node.getElement().toString();
        } else {
            return "null";
        }
    }
}
