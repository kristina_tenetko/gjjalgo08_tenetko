package com.getjavajob.training.algo08.tenetkok.lesson08.binary.search;

import com.getjavajob.training.algo08.tenetkok.lesson07.Node;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class BinarySearchTreeTest {

    public static void main(String[] args) {
        testStringRepresentation();
        testBinaryComparator();
        testTreeSearch();
        testRemoveLeaf();
        testRemoveOneChildRight();
        testRemoveOneChildLeft();
        testRemoveBothChild();
    }

    private static void testRemoveLeaf() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> n1 = tree.addRoot(4);
        Node<Integer> n11 = tree.add(n1, 2);
        Integer result = tree.remove(n11);
        assertEquals("BinarySearchTreeTest.testRemoveLeaf", "(4)", tree.stringRepresentation());
        assertEquals("BinarySearchTreeTest.testRemoveLeaf()", 2, result);
    }

    private static void testRemoveOneChildLeft() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> n1 = tree.addRoot(4);
        Node<Integer> n11 = tree.add(n1, 2);
        Node<Integer> n111 = tree.add(n11, 1);
        Integer result = tree.remove(n11);
        assertEquals("BinarySearchTreeTest.testRemoveOneChildLeft", "(4(1))", tree.stringRepresentation());
        assertEquals("BinarySearchTreeTest.testRemoveOneChildLeft()", 2, result);
    }

    private static void testRemoveOneChildRight() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> n1 = tree.addRoot(4);
        Node<Integer> n11 = tree.add(n1, 7);
        Node<Integer> n111 = tree.add(n11, 9);
        Integer result = tree.remove(n11);
        assertEquals("BinarySearchTreeTest.testRemoveOneChildRight", "(4(null, 9))", tree.stringRepresentation());
        assertEquals("BinarySearchTreeTest.testRemoveOneChildRight()", 7, result);
    }

    private static void testRemoveBothChild() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> n1 = tree.addRoot(8);
        Node<Integer> n11 = tree.add(n1, 5);
        Node<Integer> n12 = tree.add(n1, 10);
        Node<Integer> n111 = tree.add(n11, 1);
        Node<Integer> n112 = tree.add(n11, 6);
        Node<Integer> n121 = tree.add(n12, 9);
        Node<Integer> n122 = tree.add(n12, 15);
        Node<Integer> n1122 = tree.add(n112, 3);
        Integer result = tree.remove(n11);
        assertEquals("BinarySearchTreeTest.testRemoveBothChild", "(8(3(1, 6), 10(9, 15)))", tree.stringRepresentation());
        assertEquals("BinarySearchTreeTest.testRemoveBothChild()", 5, result);
    }

    private static void testTreeSearch() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> n1 = tree.addRoot(4);
        Node<Integer> n11 = tree.add(n1, 2);
        Node<Integer> n12 = tree.add(n1, 15);
        Node<Integer> n111 = tree.add(n11, 1);
        Node<Integer> n112 = tree.add(n11, 3);
        Node<Integer> n121 = tree.add(n12, 7);
        Node<Integer> n122 = tree.add(n121, 6);
        assertEquals("BinarySearchTreeTest.testTreeSearch()", 7, tree.treeSearch(n1, 7).getElement());
        assertEquals("BinarySearchTreeTest.testTreeSearch()", null, tree.treeSearch(n1, 5));
    }


    private static void testBinaryComparator() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> n1 = tree.addRoot(4);
        Node<Integer> n11 = tree.add(n1, 2);
        Node<Integer> n12 = tree.add(n1, 15);
        Node<Integer> n111 = tree.add(n11, 1);
        Node<Integer> n112 = tree.add(n11, 3);
        Node<Integer> n121 = tree.add(n12, 7);
        Node<Integer> n122 = tree.add(n121, 6);
        assertEquals("BinarySearchTreeTest.testBinaryComparator()", "(4(2(1, 3), 15(7(6))))", tree.stringRepresentation());
    }

    private static void testStringRepresentation() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        Node<Integer> n1 = tree.addRoot(8);
        Node<Integer> n11 = tree.add(n1, 5);
        Node<Integer> n12 = tree.add(n1, 10);
        Node<Integer> n111 = tree.add(n11, 1);
        Node<Integer> n112 = tree.add(n11, 6);
        Node<Integer> n121 = tree.add(n12, 9);
        Node<Integer> n122 = tree.add(n12, 15);
        assertEquals("BinarySearchTreeTest.testStringRepresentation()", "(8(5(1, 6), 10(9, 15)))", tree.stringRepresentation());
    }
}
