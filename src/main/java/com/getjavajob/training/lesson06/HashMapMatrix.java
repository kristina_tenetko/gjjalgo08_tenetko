package com.getjavajob.training.algo08.tenetkok.lesson06;

import java.util.HashMap;

public class HashMapMatrix<V> implements Matrix<V> {

    private HashMap<MatrixKey, V> matrix;

    HashMapMatrix() {
        matrix = new HashMap<>();
    }

    public V get(int i, int j) {
        return matrix.get(new MatrixKey(i, j));
    }

    public void set(int i, int j, V value) {
        matrix.put(new MatrixKey(i, j), value);
    }

    class MatrixKey {
        int i;
        int j;

        MatrixKey(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            MatrixKey key = (MatrixKey) o;
            return i == key.i && j == key.j;
        }

        @Override
        public int hashCode() {
            return 310 * i + j;
        }
    }

}
