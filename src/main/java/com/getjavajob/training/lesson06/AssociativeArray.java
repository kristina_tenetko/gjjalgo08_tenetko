package com.getjavajob.training.algo08.tenetkok.lesson06;

public class AssociativeArray<K, V> {

    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;
    private static final int MAXIMUM_CAPACITY = 1 << 30;

    private Entry<K, V>[] table = new Entry[DEFAULT_INITIAL_CAPACITY];
    private int size;
    private int threshold = (int) (DEFAULT_INITIAL_CAPACITY * DEFAULT_LOAD_FACTOR);

    public V add(K key, V val) {
        int hash = (key != null) ? hash(key) : 0;
        int bucketIndex = tableIndex(hash, table.length);
        for (Entry e = table[bucketIndex]; e != null; e = e.next) {
            if (e.key == key || e.key.equals(key)) {
                return (V) e.setValue(val);
            }
        }
        resizeTableIfNeeded(bucketIndex);
        table[bucketIndex] = new Entry(key, val, table[bucketIndex]);
        size++;

        return null;
    }

    public V get(K key) {
        int hash = (key == null) ? 0 : hash(key);
        for (Entry e = table[tableIndex(hash, table.length)]; e != null; e = e.next) {
            if (e.key == key || e.key.equals(key)) {
                return (V) e.getValue();
            }
        }
        return null;
    }

    public V remove(K key) {
        int hash = (key != null) ? hash(key) : 0;
        int i = tableIndex(hash, table.length);
        Entry prev = table[i];
        Entry next;

        for (Entry e = prev; e != null; e = next) {
            next = e.next;
            if (e.key == key || e.key.equals(key)) {
                if (prev == e) {
                    table[i] = next;
                } else {
                    prev.next = next;
                }
                return (V) e.getValue();
            }
            prev = e;
        }
        return null;
    }

    private int tableIndex(int hash, int length) {
        return Math.abs(hash % length);
    }

    private int hash(K k) {
        return k.hashCode();
    }

    private void resizeTableIfNeeded(int index) {
        if ((size >= threshold) && (table[index] != null)) {
            resize(2 * table.length);
        }
    }

    private void resize(int newCapacity) {
        if (table.length == MAXIMUM_CAPACITY) {
            threshold = Integer.MAX_VALUE;
            return;
        }

        Entry[] oldTable = table;
        table = new Entry[newCapacity];
        reindex(oldTable);
        float loadFactor = DEFAULT_LOAD_FACTOR;
        threshold = (int) Math.min(newCapacity * loadFactor, MAXIMUM_CAPACITY + 1);
    }

    private void reindex(Entry[] table) {
        int newCapacity = table.length;
        for (Entry e : table) {
            while (e != null) {
                add((K) e.getKey(), (V) e.getValue());
                e = e.next;
            }
        }
    }

    class Entry<K, V> {
        K key;
        V value;
        Entry next;

        public Entry(K key, V value, Entry next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public final V setValue(V newValue) {
            V oldValue = this.value;
            this.value = newValue;
            return oldValue;
        }

        public K getKey() {
            return this.key;
        }

        public V getValue() {
            return this.value;
        }
    }
}
