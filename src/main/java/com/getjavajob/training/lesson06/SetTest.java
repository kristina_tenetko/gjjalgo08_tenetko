package com.getjavajob.training.algo08.tenetkok.lesson06;

import java.util.HashSet;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class SetTest {

    public static void main(String[] args) {
        testHashAdd();
        testHashAddAll();
    }

    private static void testHashAddAll() {
        HashSet<Integer> hash = new HashSet<>();
        hash.add(2);
        HashSet<Integer> another = new HashSet<>();
        another.add(45);
        another.add(37);
        assertEquals("SetTest.testHashAddAll1", true, hash.addAll(another));
        HashSet<Integer> set = new HashSet<>();
        set.add(2);
        set.add(88);
        assertEquals("SetTest.testHashAddAll2", true, hash.addAll(set));
        HashSet<Integer> n = new HashSet<>();
        assertEquals("SetTest.testHashAddAll3", false, hash.addAll(n));
    }

    private static void testHashAdd() {
        HashSet<Integer> hash = new HashSet<>();
        hash.add(2);
        assertEquals("SetTest.testHashAdd1", false, hash.add(2));
        assertEquals("SetTest.testHashAdd2", true, hash.add(8));
    }
}
