package com.getjavajob.training.algo08.tenetkok.lesson06;

import java.util.LinkedHashSet;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class LinkedHashSetTest {
    public static void main(String[] args) {
        testAdd();
    }

    private static void testAdd() {
        LinkedHashSet<Integer> hashSet = new LinkedHashSet<>();
        assertEquals("LinkedHashSetTest.testAdd1", true, hashSet.add(12));
        assertEquals("LinkedHashSetTest.testAdd2", false, hashSet.add(12));
        hashSet.add(4);
        hashSet.add(7);
        assertEquals("LinkedHashSetTest.testAdd3", "[12, 4, 7]", hashSet.toString());
        hashSet.remove(4);
        hashSet.add(5);
        assertEquals("LinkedHashSetTest.testAdd4", "[12, 7, 5]", hashSet.toString());
    }
}
