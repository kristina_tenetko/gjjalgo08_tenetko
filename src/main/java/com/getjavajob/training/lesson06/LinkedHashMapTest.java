package com.getjavajob.training.algo08.tenetkok.lesson06;

import java.util.LinkedHashMap;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class LinkedHashMapTest {
    public static void main(String[] args) {
        testPut();
    }

    private static void testPut() {
        LinkedHashMap<Integer, String> hashMap = new LinkedHashMap();
        hashMap.put(0, "A");
        hashMap.put(1, "C");
        hashMap.put(2, "B");
        assertEquals("LinkedHashMapTest.testPut", "[0, 1, 2]", hashMap.keySet().toString());
        assertEquals("LinkedHashMapTest.testPut", "[A, C, B]", hashMap.values().toString());
    }
}
