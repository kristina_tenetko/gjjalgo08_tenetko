package com.getjavajob.training.algo08.tenetkok.lesson06;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class AssociativeArrayTest {

    public static void main(String[] args) {
        testAddToArray();
        testAddNullToArray();
        testAddDuplicateKey();
        testRemoveKey();
    }

    private static void testRemoveKey() {
        AssociativeArray<String, String> array = new AssociativeArray<>();
        array.add("firstKey", "firstValue");
        String value = array.remove("firstKey");
        assertEquals("AssociativeArrayTest.testRemoveKeyReturnValue", "firstValue", value);
        assertEquals("AssociativeArrayTest.testRemoveKey", null, array.get("firstKey"));
    }

    private static void testAddToArray() {
        AssociativeArray<String, String> array = new AssociativeArray<>();
        array.add("firstKey", "firstValue");
        assertEquals("AssociativeArrayTest.testAddToArray", "firstValue", array.get("firstKey"));
    }

    private static void testAddNullToArray() {
        AssociativeArray<String, String> array = new AssociativeArray<>();
        array.add(null, "firstValue");
        assertEquals("AssociativeArrayTest.testAddNullToArray", "firstValue", array.get(null));
    }

    private static void testAddDuplicateKey() {
        AssociativeArray<String, String> array = new AssociativeArray<>();
        array.add("firstKey", "firstValue");
        String oldValue = array.add("firstKey", "newFirstValue");
        assertEquals("AssociativeArrayTest.testAddDuplicateKeyReturnValue", "firstValue", oldValue);
        assertEquals("AssociativeArrayTest.testAddDuplicateKey", "newFirstValue", array.get("firstKey"));
    }
}
