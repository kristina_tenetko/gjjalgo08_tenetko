package com.getjavajob.training.algo08.tenetkok.lesson06;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class MatrixTest {

    public static void main(String[] args) {
        testMatrixFill();
    }

    private static void testMatrixFill() {
        HashMapMatrix<Integer> matrix = new HashMapMatrix<>();
        int n = 1_000_000;
        List<Integer> compare = new ArrayList<>();
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < 1_000_000; i++) {
            matrix.set(i, n, i);
            compare.add(i);
        }
        for (int i = 0; i < 1_000_000; i++) {
            result.add(matrix.get(i, n));
        }
        assertEquals("MatrixTest.testMillionElementsInMillionRow", true, result.equals(compare));
    }
}
