package com.getjavajob.training.algo08.tenetkok.lesson06;

import java.util.HashMap;

import static com.getjavajob.training.algo08.util.StopWatch.getElapsedTime;
import static com.getjavajob.training.algo08.util.StopWatch.start;

public class AssociativeArrayPerformanceTest {

    static final int NUMBER_OF_ELEMENTS = 2_000_000;

    public static void main(String[] args) {
        AssociativeArray<Integer, Integer> myAssociativeArray = new AssociativeArray<>();
        HashMap<Integer, Integer> map = new HashMap<>();
        testPutMethod(myAssociativeArray);
        testPutHashMap(map);
        testGetMethod(myAssociativeArray);
        testGetHashMap(map);
        testRemoveMethod(myAssociativeArray);
        testRemoveHashMap(map);

    }

    public static void testPutMethod(AssociativeArray<Integer, Integer> associativeArray) {
        System.out.println("-----------Addition-----------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            associativeArray.add(i, i);
        }
        System.out.println("AssociativeArray.add(k, v): " + getElapsedTime() + " ms");
    }

    public static void testPutHashMap(HashMap<Integer, Integer> hashMap) {
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            hashMap.put(i, i);
        }
        System.out.println("HashMap.put(k,v): " + getElapsedTime() + " ms");
    }

    public static void testGetMethod(AssociativeArray<Integer, Integer> associativeArray) {
        System.out.println("-----------Getting-----------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            associativeArray.get(i);
        }
        System.out.println("AssociativeArray.get(k): " + getElapsedTime() + " ms");
    }

    public static void testGetHashMap(HashMap<Integer, Integer> hashMap) {
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            hashMap.get(i);
        }
        System.out.println("HashMap.get(k): " + getElapsedTime() + " ms");
    }

    public static void testRemoveMethod(AssociativeArray<Integer, Integer> associativeArray) {
        System.out.println("-----------Removing-----------");
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            associativeArray.remove(i);
        }
        System.out.println("AssociativeArray.remove(k): " + getElapsedTime() + " ms");
    }

    public static void testRemoveHashMap(HashMap<Integer, Integer> hashMap) {
        start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            hashMap.remove(i);
        }
        System.out.println("HashMap.remove(k): " + getElapsedTime() + " ms");
    }
}

