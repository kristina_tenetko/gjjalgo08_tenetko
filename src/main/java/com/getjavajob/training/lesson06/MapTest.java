package com.getjavajob.training.algo08.tenetkok.lesson06;

import java.util.*;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class MapTest {
    public static void main(String[] args) {
        HashMap<String, Integer> map = new HashMap<>();

        assertEquals("MapTest.isEmpty()", true, map.isEmpty());
        assertEquals("MapTest.putValue()", null, map.put("putOne", 1));
        assertEquals("MapTest.containsKey()", true, map.containsKey("putOne"));
        assertEquals("MapTest.containsValue()", true, map.containsValue(1));
        assertEquals("MapTest.getValue()", 1, map.get("putOne"));
        assertEquals("MapTest.putValueDuplicateKey", 1, map.put("putOne", 2));
        assertEquals("MapTest.putValueNewKey", null, map.put("newKey", 27));
        assertEquals("MapTest.size()", 2, map.size());
        assertEquals("MapTest.remove", 27, map.remove("newKey"));
        assertEquals("MapTest.removeNoAddedKey", null, map.remove("noKey"));
        HashMap<String, Integer> mapPutAll = new HashMap<>();
        mapPutAll.put("mapPutAll1", 1);
        mapPutAll.put("mapPutAll2", 2);
        mapPutAll.put("putOne", 3);
        map.putAll(mapPutAll);
        assertEquals("MapTest.putAll()_check1", 1, map.get("mapPutAll1"));
        assertEquals("MapTest.putAll()_check2", 2, map.get("mapPutAll2"));
        assertEquals("MapTest.putAll()_check3", 3, map.get("putOne"));
        assertEquals("MapTest.putAll()_sizeCheck", 3, map.size());
        mapPutAll.clear();
        assertEquals("MapTest.clear()", 0, mapPutAll.size());

        List<String> keys = new ArrayList<>(map.keySet());
        Collections.sort(keys);
        List<String> expectedKeys = new ArrayList<>();
        expectedKeys.add("mapPutAll1");
        expectedKeys.add("mapPutAll2");
        expectedKeys.add("putOne");
        assertEquals("MapTest.keySet()", expectedKeys, keys);

        Collection<Integer> values = map.values();
        List<Integer> list = new ArrayList<>(values);
        Collections.sort(list);
        List<Integer> expectedValues = new ArrayList<>();
        expectedValues.add(1);
        expectedValues.add(2);
        expectedValues.add(3);
        assertEquals("MapTest.values()", expectedValues, list);
    }
}
